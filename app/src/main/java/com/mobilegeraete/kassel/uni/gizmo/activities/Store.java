package com.mobilegeraete.kassel.uni.gizmo.activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.mobilegeraete.kassel.uni.gizmo.Constants;
import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.Variables;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Delivery;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Item;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;

import java.util.ArrayList;
import java.util.HashSet;

public class Store extends AppCompatActivity {
    // declaration of the CountDownTimers
    CountDownTimer buyButtonTimer;
    CountDownTimer countDownTimerShop;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);

        //Set up actionBar for Store activity
        //Retrieve a reference to this activity's ActionBar.
        ActionBar actionBar = getSupportActionBar();
        //Set up the apps logo in the actionbar.
        actionBar.setLogo(R.mipmap.shop);
        //Set up the activities title in the action bar.
        actionBar.setTitle(getString(R.string.storeTitle));
        //Set whether to display the activity logo rather than the activity icon.
        actionBar.setDisplayUseLogoEnabled(true);
        //Set whether to include the application home affordance in the action bar.
        actionBar.setDisplayShowHomeEnabled(true);
    }

    protected void onResume() {
        super.onResume();
        // check if buy process is not currently running.
        // If it is not running the disableCheckBoxesAtMaxCapacitiy() method will be called.
        if (!Variables.buyProcessRunning) {
            disableCheckBoxesAtMaxCapacitiy();
        }

        // check if buy process is currently running.
        // If it is running the disableCheckboxesAfterBuyButtonPressed() method will be called
        // and the dummyTimer will be started to show the timer again
        // because the timer has been stopped and the buy button will not be clickable
        // again until buy process is finished.
        if (Variables.buyProcessRunning) {
            disableCheckboxesAfterBuyButtonPressed();
            backInStoreTimer();
        }

        // creates usable check boxes connected to the XML checkboxes
        CheckBox checkBoxMeal1 = (CheckBox) findViewById(R.id.checkBoxMeal1);
        CheckBox checkBoxMeal2 = (CheckBox) findViewById(R.id.checkBoxMeal2);
        CheckBox checkBoxMeal3 = (CheckBox) findViewById(R.id.checkBoxMeal3);
        CheckBox checkBoxMeal4 = (CheckBox) findViewById(R.id.checkBoxMeal4);
        CheckBox checkBoxMeal5 = (CheckBox) findViewById(R.id.checkBoxMeal5);
        CheckBox checkBoxMeal6 = (CheckBox) findViewById(R.id.checkBoxMeal6);
        CheckBox checkBoxMeal7 = (CheckBox) findViewById(R.id.checkBoxMeal7);
        CheckBox checkBoxMeal8 = (CheckBox) findViewById(R.id.checkBoxMeal8);
        CheckBox checkBoxMeal9 = (CheckBox) findViewById(R.id.checkBoxMeal9);
        CheckBox checkBoxDrink1 = (CheckBox) findViewById(R.id.checkBoxDrink1);
        CheckBox checkBoxDrink2 = (CheckBox) findViewById(R.id.checkBoxDrink2);
        CheckBox checkBoxDrink3 = (CheckBox) findViewById(R.id.checkBoxDrink3);
        CheckBox checkBoxWashingAgent = (CheckBox) findViewById(R.id.checkBoxWashingAgent);
        CheckBox checkBoxDishSoap = (CheckBox) findViewById(R.id.checkBoxDishsoap);

        // deactivate checkboxes while waiting for delivery
        // this part is used when a buy process is running
        // and the user switched back to the store without exiting the application
        // The lifecycle of Variables is NOT extended beyond the lifecycle of the application
        if (Variables.buyProcessRunning) {
            for (Item i : Variables.selections) {
                if (i.getName().equals(this.getString(R.string.startActivityMeal1))) {
                    checkBoxMeal1.setEnabled(true);
                    checkBoxMeal1.setChecked(true);
                    checkBoxMeal1.setEnabled(false);
                }
                if (i.getName().equals(this.getString(R.string.startActivityMeal2))) {
                    checkBoxMeal2.setEnabled(true);
                    checkBoxMeal2.setChecked(true);
                    checkBoxMeal2.setEnabled(false);
                }
                if (i.getName().equals(this.getString(R.string.startActivityMeal3))) {
                    checkBoxMeal3.setEnabled(true);
                    checkBoxMeal3.setChecked(true);
                    checkBoxMeal3.setEnabled(false);
                }
                if (i.getName().equals(this.getString(R.string.startActivityMeal4))) {
                    checkBoxMeal4.setEnabled(true);
                    checkBoxMeal4.setChecked(true);
                    checkBoxMeal4.setEnabled(false);
                }
                if (i.getName().equals(this.getString(R.string.startActivityMeal5))) {
                    checkBoxMeal5.setEnabled(true);
                    checkBoxMeal5.setChecked(true);
                    checkBoxMeal5.setEnabled(false);
                }
                if (i.getName().equals(this.getString(R.string.startActivityMeal6))) {
                    checkBoxMeal6.setEnabled(true);
                    checkBoxMeal6.setChecked(true);
                    checkBoxMeal6.setEnabled(false);
                }
                if (i.getName().equals(this.getString(R.string.startActivityMeal7))) {
                    checkBoxMeal7.setEnabled(true);
                    checkBoxMeal7.setChecked(true);
                    checkBoxMeal7.setEnabled(false);
                }
                if (i.getName().equals(this.getString(R.string.startActivityMeal8))) {
                    checkBoxMeal8.setEnabled(true);
                    checkBoxMeal8.setChecked(true);
                    checkBoxMeal8.setEnabled(false);
                }
                if (i.getName().equals(this.getString(R.string.startActivityMeal9))) {
                    checkBoxMeal9.setEnabled(true);
                    checkBoxMeal9.setChecked(true);
                    checkBoxMeal9.setEnabled(false);
                }
                if (i.getName().equals(this.getString(R.string.startActivityDrink1))) {
                    checkBoxDrink1.setEnabled(true);
                    checkBoxDrink1.setChecked(true);
                    checkBoxDrink1.setEnabled(false);
                }
                if (i.getName().equals(this.getString(R.string.startActivityDrink2))) {
                    checkBoxDrink2.setEnabled(true);
                    checkBoxDrink2.setChecked(true);
                    checkBoxDrink2.setEnabled(false);
                }
                if (i.getName().equals(this.getString(R.string.startActivityDrink3))) {
                    checkBoxDrink3.setEnabled(true);
                    checkBoxDrink3.setChecked(true);
                    checkBoxDrink3.setEnabled(false);
                }
                if (i.getName().equals(this.getString(R.string.startActivityClean1))) {
                    checkBoxWashingAgent.setEnabled(true);
                    checkBoxWashingAgent.setChecked(true);
                    checkBoxWashingAgent.setEnabled(false);
                }
                if (i.getName().equals(this.getString(R.string.startActivityClean2))) {
                    checkBoxDishSoap.setEnabled(true);
                    checkBoxDishSoap.setChecked(true);
                    checkBoxDishSoap.setEnabled(false);
                }
            }
        }

        // deactivate checkboxes while waiting for delivery
        // this part is used when a buy process is running
        // and the user switched back to the store with exiting the application beforehand
        // The lifecycle of persistence data is extended beyond the lifecycle of the application
        if (Persistence.getDlw().isBuyProcessStarted()) {
            for (String name : Persistence.getDlw().getDelivery().getItems()) {
                if (name.equals(this.getString(R.string.startActivityMeal1))) {
                    checkBoxMeal1.setEnabled(true);
                    checkBoxMeal1.setChecked(true);
                    checkBoxMeal1.setEnabled(false);
                }
                if (name.equals(this.getString(R.string.startActivityMeal2))) {
                    checkBoxMeal2.setEnabled(true);
                    checkBoxMeal2.setChecked(true);
                    checkBoxMeal2.setEnabled(false);
                }
                if (name.equals(this.getString(R.string.startActivityMeal3))) {
                    checkBoxMeal3.setEnabled(true);
                    checkBoxMeal3.setChecked(true);
                    checkBoxMeal3.setEnabled(false);
                }
                if (name.equals(this.getString(R.string.startActivityMeal4))) {
                    checkBoxMeal4.setEnabled(true);
                    checkBoxMeal4.setChecked(true);
                    checkBoxMeal4.setEnabled(false);
                }
                if (name.equals(this.getString(R.string.startActivityMeal5))) {
                    checkBoxMeal5.setEnabled(true);
                    checkBoxMeal5.setChecked(true);
                    checkBoxMeal5.setEnabled(false);
                }
                if (name.equals(this.getString(R.string.startActivityMeal6))) {
                    checkBoxMeal6.setEnabled(true);
                    checkBoxMeal6.setChecked(true);
                    checkBoxMeal6.setEnabled(false);
                }
                if (name.equals(this.getString(R.string.startActivityMeal7))) {
                    checkBoxMeal7.setEnabled(true);
                    checkBoxMeal7.setChecked(true);
                    checkBoxMeal7.setEnabled(false);
                }
                if (name.equals(this.getString(R.string.startActivityMeal8))) {
                    checkBoxMeal8.setEnabled(true);
                    checkBoxMeal8.setChecked(true);
                    checkBoxMeal8.setEnabled(false);
                }
                if (name.equals(this.getString(R.string.startActivityMeal9))) {
                    checkBoxMeal9.setEnabled(true);
                    checkBoxMeal9.setChecked(true);
                    checkBoxMeal9.setEnabled(false);
                }
                if (name.equals(this.getString(R.string.startActivityDrink1))) {
                    checkBoxDrink1.setEnabled(true);
                    checkBoxDrink1.setChecked(true);
                    checkBoxDrink1.setEnabled(false);
                }
                if (name.equals(this.getString(R.string.startActivityDrink2))) {
                    checkBoxDrink2.setEnabled(true);
                    checkBoxDrink2.setChecked(true);
                    checkBoxDrink2.setEnabled(false);
                }
                if (name.equals(this.getString(R.string.startActivityDrink3))) {
                    checkBoxDrink3.setEnabled(true);
                    checkBoxDrink3.setChecked(true);
                    checkBoxDrink3.setEnabled(false);
                }
                if (name.equals(this.getString(R.string.startActivityClean1))) {
                    checkBoxWashingAgent.setEnabled(true);
                    checkBoxWashingAgent.setChecked(true);
                    checkBoxWashingAgent.setEnabled(false);
                }
                if (name.equals(this.getString(R.string.startActivityClean2))) {
                    checkBoxDishSoap.setEnabled(true);
                    checkBoxDishSoap.setChecked(true);
                    checkBoxDishSoap.setEnabled(false);
                }
            }
        }
    }

    public void onPause() {
        super.onPause();
        /* ---------------------------------------------------------------------------
        when using on screen buttons (back to home & overview)
        the amount of selected items are reset.
        Also while checkboxes are checked and no buy process is running
        these boxes will be unchecked and the selections will be cleared.
        ----------------------------------------------------------------------------*/

        if (Variables.countSelectedItems > 1 && !Variables.buyProcessRunning) {
            removeSelectionAfterBuyProcessFinished();
            Variables.selections.clear();
        }
            Variables.countSelectedItems = 1;


        // cancel countDownTimerShop if it is running
        if (Variables.countDownTimerActive) {
            Variables.countDownTimerActive = false;
            countDownTimerShop.cancel();

        }

        // cancel shoppingTimer if it is running
        if (Variables.buyButtonTimerActive) {
            Variables.buyButtonTimerActive = false;
            buyButtonTimer.cancel();
        }
    }

    /* ---------------------------------------------------------------------------
    onClickBuyButton() method provides the button with a onClick function.
    This method creates a delivery object and adds the selected items to it.
    The delivery will be saved and is not lost after exiting the App.
    A count down timer will be created and write the remaining time into a TextView
    while the shop activity is still opened.
    Clears text field onFinish and resets variables to the state
    before a buy process has started.
    ----------------------------------------------------------------------------*/
    public void onClickBuyButton(View v) {
        // only start buy process if no buy process is currently running
        // and at least one item to buy is selected
        if (!Persistence.getDlw().isBuyProcessStarted() && Variables.countSelectedItems > 1) {
            // remove duplicates in selection
            checkDoubleEntriesInSelection(Variables.selections);

            // crate delivery object with delivery time & selected items
            // and save it via persistence class
            Delivery delivery = new Delivery().withTime(System.currentTimeMillis() + Constants.SHOPPING_DURATION);
            for (Item i : Variables.selections) {
                delivery.getItems().add(i.getName());
            }
            Persistence.getDlw().setDelivery(delivery);

            // set important variables
            Variables.buyProcessRunning = true;
            Persistence.getDlw().setBuyProcessStarted(true);
            Variables.buyButtonTimerActive = true;
            final TextView timerBuyButton = (TextView) findViewById(R.id.storeTimerTV);
            timerBuyButton.setTypeface(null, Typeface.BOLD);

            // disable checkboxes
            disableCheckboxesAfterBuyButtonPressed();
            Log.d("OnClickBuyButton", "Bevor der Timer startet: " + Variables.remainingTime);

            // CountDownTimer for buy duration duration
            // The duration is broken down to minutes and seconds for improved readability.
            buyButtonTimer = new CountDownTimer(Variables.remainingTime, 1000) {
                public void onTick(long millisUntilFinished) {
                    Log.d("OnClickBuyButton", "RemainingTime" + millisUntilFinished);
                    int min = (int) (millisUntilFinished / 60000) % 60;
                    int sec = (int) (millisUntilFinished / 1000) % 60;
                    if (min < 10 && sec < 10) {
                        timerBuyButton.setTypeface(null, Typeface.BOLD);
                        timerBuyButton.setText("0" + min + ":" + "0" + sec);
                    } else if (min < 10 && sec > 10) {
                        timerBuyButton.setTypeface(null, Typeface.BOLD);
                        timerBuyButton.setText("0" + min + ":" + sec);
                    } else if (min > 10 && sec < 10) {
                        timerBuyButton.setTypeface(null, Typeface.BOLD);
                        timerBuyButton.setText(min + ":" + "0" + sec);
                    } else {
                        timerBuyButton.setTypeface(null, Typeface.BOLD);
                        timerBuyButton.setText(min + ":" + sec);
                    }
                }

                // Clears text field onFinish and resets variables to the state
                // before a buy process has started. Also enables viable checkboxes.
                public void onFinish() {
                    // clear textView
                    timerBuyButton.setText("");
                    Log.d("OnClickBuyButton", "Time after onFinish: " + Variables.remainingTime);

                    // reset variables after buy process
                    Persistence.getDlw().setBuyProcessStarted(false);
                    Variables.buyProcessRunning = false;
                    Variables.countSelectedItems = 1;
                    Variables.buyButtonTimerActive = true;

                    // enables viable checkboxes
                    enableCheckBoxes();
                    removeSelectionAfterBuyProcessFinished();
                    disableCheckBoxesAtMaxCapacitiy();
                }
            }.start();
        }
    }

    /* ---------------------------------------------------------------------------
    backInStoreTimer() starts a count down timer and write the remaining time
    into a TextView.
    This method is used to show the timer when the user enters the shop activity
    after exiting the App or switching into another activity and back to the shop.
    Clears text field onFinish and resets variables to the state
    before a buy process has started.
   ----------------------------------------------------------------------------*/
    public void backInStoreTimer() {
        disableCheckBoxesAtMaxCapacitiy();
        Variables.countDownTimerActive = true;
        final TextView timerBuyButton = (TextView) findViewById(R.id.storeTimerTV);

        // countDownTimerShop for buy duration duration
        // The duration is broken down to minutes and seconds for improved readability.
        countDownTimerShop = new CountDownTimer(Variables.remainingTime + 300, 1000) {
            public void onTick(long millisUntilFinished) {
                Log.d("dummy", "" + millisUntilFinished);
                int min = (int) (millisUntilFinished / 60000) % 60;
                int sec = (int) (millisUntilFinished / 1000) % 60;
                if (min < 10 && sec < 10) {
                    timerBuyButton.setTypeface(null, Typeface.BOLD);
                    timerBuyButton.setText("0" + min + ":" + "0" + sec);
                } else if (min < 10 && sec > 10) {
                    timerBuyButton.setTypeface(null, Typeface.BOLD);
                    timerBuyButton.setText("0" + min + ":" + sec);
                } else if (min > 10 && sec < 10) {
                    timerBuyButton.setTypeface(null, Typeface.BOLD);
                    timerBuyButton.setText(min + ":" + "0" + sec);
                } else {
                    timerBuyButton.setTypeface(null, Typeface.BOLD);
                    timerBuyButton.setText(min + ":" + sec);
                }
            }

            // onFinish call if counter terminates while shop activity is open.
            // Clears text field onFinish and resets variables to the state
            // before a buy process has started. Also enables viable checkboxes.
            public void onFinish() {
                // clear textView
                timerBuyButton.setText("");

                // reset variables after buy process
                Variables.countDownTimerActive = false;
                Persistence.getDlw().setBuyProcessStarted(false);
                Variables.countSelectedItems = 1;

                // enables viable checkboxes
                enableCheckBoxes();
                removeSelectionAfterBuyProcessFinished();
                disableCheckBoxesAtMaxCapacitiy();
            }
        }.start();
    }

    /* ---------------------------------------------------------------------------
    onClick functionality for backToMainMenu() button
    On click the button will call the mainMenu activity and set needed variables.
    Both countDownTimers will be canceled as well.
   ----------------------------------------------------------------------------*/
    public void backToMainMenu(View v) {
        Intent intent = new Intent(this, MainMenu.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        Variables.countSelectedItems = 1;
        // To prevent an additional LOOP thread the switch back to mainMenu has to be saved
        Variables.backToMainMenu = true;

        // cancel countDownTimerShop if it is running
        if (Variables.countDownTimerActive) {
            Variables.countDownTimerActive = false;
            countDownTimerShop.cancel();
        }

        // cancel shoppingTimer if it is running
        if (Variables.buyButtonTimerActive) {
            Variables.buyButtonTimerActive = false;
            buyButtonTimer.cancel();
        }
    }

    /* ---------------------------------------------------------------------------
    onClick functionality for the checkboxes in the store activity
    On click the checkbox will be checked, the counter will be incremented
    and the corresponding item will be added to Variables.selections.
    On click a checked checkbox will be unchecked, the counter will be decremented
    and the corresponding item will be removed from Variables.selections.
    ----------------------------------------------------------------------------*/
    public void onClickSelectItem(View v) {
        boolean checked = ((CheckBox) v).isChecked();

        // all disableCheckBoxes in the store are needed to be disabled (make it grey!)
        // if the user selected 5 items.

        // all checkboxes in the store
        CheckBox checkBoxMeal1 = (CheckBox) findViewById(R.id.checkBoxMeal1);
        CheckBox checkBoxMeal2 = (CheckBox) findViewById(R.id.checkBoxMeal2);
        CheckBox checkBoxMeal3 = (CheckBox) findViewById(R.id.checkBoxMeal3);
        CheckBox checkBoxMeal4 = (CheckBox) findViewById(R.id.checkBoxMeal4);
        CheckBox checkBoxMeal5 = (CheckBox) findViewById(R.id.checkBoxMeal5);
        CheckBox checkBoxMeal6 = (CheckBox) findViewById(R.id.checkBoxMeal6);
        CheckBox checkBoxMeal7 = (CheckBox) findViewById(R.id.checkBoxMeal7);
        CheckBox checkBoxMeal8 = (CheckBox) findViewById(R.id.checkBoxMeal8);
        CheckBox checkBoxMeal9 = (CheckBox) findViewById(R.id.checkBoxMeal9);
        CheckBox checkBoxDrink1 = (CheckBox) findViewById(R.id.checkBoxDrink1);
        CheckBox checkBoxDrink2 = (CheckBox) findViewById(R.id.checkBoxDrink2);
        CheckBox checkBoxDrink3 = (CheckBox) findViewById(R.id.checkBoxDrink3);
        CheckBox checkBoxWashingAgent = (CheckBox) findViewById(R.id.checkBoxWashingAgent);
        CheckBox checkBoxDishSoap = (CheckBox) findViewById(R.id.checkBoxDishsoap);

        // check if the number of already selected checkboxes are higher than our maxselections


        // space is available in the shopping cart, add selected items
        switch (v.getId()) {
            //case 1: Bohnen und Speck
            case R.id.checkBoxMeal1:

                //check if checkbox is selected
                if (checked) {
                    Variables.selections.add(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal1)));
                    Variables.countSelectedItems++;
                } else {
                    Variables.selections.remove(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal1)));
                    Variables.countSelectedItems--;
                    enableCheckBoxes();

                }

                break;

            //case 2: Knusperfloeckchen
            case R.id.checkBoxMeal2:

                //check if checkbox is selected
                if (checked) {
                    Variables.selections.add(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal2)));
                    Variables.countSelectedItems++;
                } else {
                    Variables.selections.remove(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal2)));
                    Variables.countSelectedItems--;
                    enableCheckBoxes();
                }

                break;
            //case 3: Wurzelwurstbrot
            case R.id.checkBoxMeal3:

                //check if checkbox is selected
                if (checked) {
                    Variables.selections.add(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal3)));
                    Variables.countSelectedItems++;
                } else {
                    Variables.selections.remove(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal3)));
                    Variables.countSelectedItems--;
                    enableCheckBoxes();

                }
                break;
            //case 4: eisbein mit Sauerkraut
            case R.id.checkBoxMeal4:

                //check if checkbox is selected
                if (checked) {
                    Variables.selections.add(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal4)));
                    Variables.countSelectedItems++;
                } else {
                    Variables.selections.remove(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal4)));
                    Variables.countSelectedItems--;
                    enableCheckBoxes();
                }

                break;

            //case 5: Sulperknochen
            case R.id.checkBoxMeal5:

                //check if checkbox is selected
                if (checked) {
                    Variables.selections.add(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal5)));
                    Variables.countSelectedItems++;
                } else {
                    Variables.selections.remove(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal5)));
                    Variables.countSelectedItems--;
                    enableCheckBoxes();
                }

                break;
            //case 6: Grüne Soße
            case R.id.checkBoxMeal6:

                //check if checkbox is selected
                if (checked) {
                    Variables.selections.add(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal6)));
                    Variables.countSelectedItems++;
                } else {
                    Variables.selections.remove(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal6)));
                    Variables.countSelectedItems--;
                    enableCheckBoxes();
                }

                break;
            //case 7: Speckkuchen
            case R.id.checkBoxMeal7:

                //check if checkbox is selected
                if (checked) {
                    Variables.selections.add(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal7)));
                    Variables.countSelectedItems++;
                } else {
                    Variables.selections.remove(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal7)));
                    Variables.countSelectedItems--;
                    enableCheckBoxes();
                }

                break;

            //case 8: Ahlewurscht
            case R.id.checkBoxMeal8:

                //check if checkbox is selected
                if (checked) {
                    Variables.selections.add(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal8)));
                    Variables.countSelectedItems++;
                } else {
                    Variables.selections.remove(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal8)));
                    Variables.countSelectedItems--;
                    enableCheckBoxes();
                }

                break;
            //case 9: Hachbrötchen
            case R.id.checkBoxMeal9:

                //check if checkbox is selected
                if (checked) {
                    Variables.selections.add(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal9)));
                    Variables.countSelectedItems++;
                } else {
                    Variables.selections.remove(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal9)));
                    Variables.countSelectedItems--;
                    enableCheckBoxes();
                }

                break;

            //case 10: Kakao
            case R.id.checkBoxDrink1:

                //check if checkbox is selected
                if (checked) {
                    Variables.selections.add(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink1)));
                    Variables.countSelectedItems++;
                } else {
                    Variables.selections.remove(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink1)));
                    Variables.countSelectedItems--;
                    enableCheckBoxes();
                }

                break;
            //case 11: Weissbier
            case R.id.checkBoxDrink2:

                //check if checkbox is selected
                if (checked) {
                    Variables.selections.add(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink2)));
                    Variables.countSelectedItems++;
                } else {
                    Variables.selections.remove(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink2)));
                    Variables.countSelectedItems--;
                    enableCheckBoxes();
                }

                break;
            //case 12: Doppelkorn
            case R.id.checkBoxDrink3:

                //check if checkbox is selected
                if (checked) {
                    Variables.selections.add(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink3)));
                    Variables.countSelectedItems++;
                } else {
                    Variables.selections.remove(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink3)));
                    Variables.countSelectedItems--;
                    enableCheckBoxes();
                }

                break;
            //case 13: Waschmittel
            case R.id.checkBoxWashingAgent:

                //check if checkbox is selected
                if (checked) {
                    Variables.selections.add(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityClean1)));
                    Variables.countSelectedItems++;
                } else {
                    Variables.selections.remove(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityClean1)));
                    Variables.countSelectedItems--;
                    enableCheckBoxes();
                }

                break;
            //case 14: Spülmittel
            case R.id.checkBoxDishsoap:

                //check if checkbox is selected
                if (checked) {
                    Variables.selections.add(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityClean2)));
                    Variables.countSelectedItems++;
                } else {
                    Variables.selections.remove(Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityClean2)));
                    Variables.countSelectedItems--;
                    enableCheckBoxes();
                }

                break;
        }
        //}
        // for every if, we need to check which checkbox is not selected and if it's not the case we disable the checkbox
        if (Variables.countSelectedItems > Constants.MAX_SELECTIONS) {

            // checkboxes FOOD
            if (!checkBoxMeal1.isChecked()) {
                checkBoxMeal1.setEnabled(false);
            }

            if (!checkBoxMeal2.isChecked()) {
                checkBoxMeal2.setEnabled(false);
            }

            if (!checkBoxMeal3.isChecked()) {
                checkBoxMeal3.setEnabled(false);
            }

            if (!checkBoxMeal4.isChecked()) {
                checkBoxMeal4.setEnabled(false);
            }

            if (!checkBoxMeal5.isChecked()) {
                checkBoxMeal5.setEnabled(false);
            }

            if (!checkBoxMeal6.isChecked()) {
                checkBoxMeal6.setEnabled(false);
            }

            if (!checkBoxMeal7.isChecked()) {
                checkBoxMeal7.setEnabled(false);
            }

            if (!checkBoxMeal8.isChecked()) {
                checkBoxMeal8.setEnabled(false);
            }

            if (!checkBoxMeal9.isChecked()) {
                checkBoxMeal9.setEnabled(false);
            }
            // checkBoxes Drinks
            if (!checkBoxDrink1.isChecked()) {
                checkBoxDrink1.setEnabled(false);
            }

            if (!checkBoxDrink2.isChecked()) {
                checkBoxDrink2.setEnabled(false);
            }

            if (!checkBoxDrink3.isChecked()) {
                checkBoxDrink3.setEnabled(false);
            }

            // checkBoxes Drinks
            if (!checkBoxWashingAgent.isChecked()) {
                checkBoxWashingAgent.setEnabled(false);
            }

            if (!checkBoxDishSoap.isChecked()) {
                checkBoxDishSoap.setEnabled(false);
            }


        }
    }

    /* ---------------------------------------------------------------------------
    enableCheckBoxes() method will enable all viable checkboxes
    if the corresponding item is below max capacity.
    ----------------------------------------------------------------------------*/
    public void enableCheckBoxes() {
        CheckBox checkBoxMeal1 = (CheckBox) findViewById(R.id.checkBoxMeal1);
        CheckBox checkBoxMeal2 = (CheckBox) findViewById(R.id.checkBoxMeal2);
        CheckBox checkBoxMeal3 = (CheckBox) findViewById(R.id.checkBoxMeal3);
        CheckBox checkBoxMeal4 = (CheckBox) findViewById(R.id.checkBoxMeal4);
        CheckBox checkBoxMeal5 = (CheckBox) findViewById(R.id.checkBoxMeal5);
        CheckBox checkBoxMeal6 = (CheckBox) findViewById(R.id.checkBoxMeal6);
        CheckBox checkBoxMeal7 = (CheckBox) findViewById(R.id.checkBoxMeal7);
        CheckBox checkBoxMeal8 = (CheckBox) findViewById(R.id.checkBoxMeal8);
        CheckBox checkBoxMeal9 = (CheckBox) findViewById(R.id.checkBoxMeal9);
        CheckBox checkBoxDrink1 = (CheckBox) findViewById(R.id.checkBoxDrink1);
        CheckBox checkBoxDrink2 = (CheckBox) findViewById(R.id.checkBoxDrink2);
        CheckBox checkBoxDrink3 = (CheckBox) findViewById(R.id.checkBoxDrink3);
        CheckBox checkBoxWashingAgent = (CheckBox) findViewById(R.id.checkBoxWashingAgent);
        CheckBox checkBoxDishSoap = (CheckBox) findViewById(R.id.checkBoxDishsoap);

        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal1)).getCount() < Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal1.setEnabled(true);
        }
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal2)).getCount() < Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal2.setEnabled(true);
        }
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal3)).getCount() < Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal3.setEnabled(true);
        }
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal4)).getCount() < Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal4.setEnabled(true);
        }
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal5)).getCount() < Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal5.setEnabled(true);
        }
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal6)).getCount() < Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal6.setEnabled(true);
        }
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal7)).getCount() < Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal7.setEnabled(true);
        }
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal8)).getCount() < Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal8.setEnabled(true);
        }
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal9)).getCount() < Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal9.setEnabled(true);
        }
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink1)).getCount() < Constants.MAX_DRINKS_SUPPLIES) {
            checkBoxDrink1.setEnabled(true);
        }
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink2)).getCount() < Constants.MAX_DRINKS_SUPPLIES) {
            checkBoxDrink2.setEnabled(true);
        }
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink3)).getCount() < Constants.MAX_DRINKS_SUPPLIES) {
            checkBoxDrink3.setEnabled(true);
        }
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityClean1)).getCount() < Constants.MAX_STUFF_SUPPLIES) {
            checkBoxWashingAgent.setEnabled(true);
        }
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityClean2)).getCount() < Constants.MAX_STUFF_SUPPLIES) {
            checkBoxDishSoap.setEnabled(true);
        }

    }

    /* ---------------------------------------------------------------------------
    disableCheckBoxesAtMaxCapacitiy() method will disable checkboxes
    if the corresponding item is at max capacity.
    Unchecks the checkbox before disabling it.
    ----------------------------------------------------------------------------*/
    public void disableCheckBoxesAtMaxCapacitiy() {
        // CheckBoxes for food buying
        // Food 1
        CheckBox checkBoxMeal1 = (CheckBox) findViewById(R.id.checkBoxMeal1);
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal1)).getCount() == Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal1.setChecked(false);
            checkBoxMeal1.setEnabled(false);
        }
        // Food 2
        CheckBox checkBoxMeal2 = (CheckBox) findViewById(R.id.checkBoxMeal2);
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal2)).getCount() == Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal2.setChecked(false);
            checkBoxMeal2.setEnabled(false);
        }
        // Food 3
        CheckBox checkBoxMeal3 = (CheckBox) findViewById(R.id.checkBoxMeal3);
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal3)).getCount() == Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal3.setChecked(false);
            checkBoxMeal3.setEnabled(false);
        }
        // Food 4
        CheckBox checkBoxMeal4 = (CheckBox) findViewById(R.id.checkBoxMeal4);
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal4)).getCount() == Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal4.setChecked(false);
            checkBoxMeal4.setEnabled(false);
        }
        // Food 5
        CheckBox checkBoxMeal5 = (CheckBox) findViewById(R.id.checkBoxMeal5);
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal5)).getCount() == Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal5.setChecked(false);
            checkBoxMeal5.setEnabled(false);
        }
        // Food 6
        CheckBox checkBoxMeal6 = (CheckBox) findViewById(R.id.checkBoxMeal6);
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal6)).getCount() == Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal6.setChecked(false);
            checkBoxMeal6.setEnabled(false);
        }
        // Food 7
        CheckBox checkBoxMeal7 = (CheckBox) findViewById(R.id.checkBoxMeal7);
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal7)).getCount() == Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal7.setChecked(false);
            checkBoxMeal7.setEnabled(false);
        }
        // Food 8
        CheckBox checkBoxMeal8 = (CheckBox) findViewById(R.id.checkBoxMeal8);
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal8)).getCount() == Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal8.setChecked(false);
            checkBoxMeal8.setEnabled(false);
        }
        // Food 9
        CheckBox checkBoxMeal9 = (CheckBox) findViewById(R.id.checkBoxMeal9);
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal9)).getCount() == Constants.MAX_MEALS_SUPPLIES) {
            checkBoxMeal9.setChecked(false);
            checkBoxMeal9.setEnabled(false);
        }

        // CheckBoxes for drink buying
        // Drink 1
        CheckBox checkBoxDrink1 = (CheckBox) findViewById(R.id.checkBoxDrink1);
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink1)).getCount() == Constants.MAX_DRINKS_SUPPLIES) {
            checkBoxDrink1.setChecked(false);
            checkBoxDrink1.setEnabled(false);
        }
        // Drink 2
        CheckBox checkBoxDrink2 = (CheckBox) findViewById(R.id.checkBoxDrink2);
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink2)).getCount() == Constants.MAX_DRINKS_SUPPLIES) {
            checkBoxDrink2.setChecked(false);
            checkBoxDrink2.setEnabled(false);
        }
        // Drink 3
        CheckBox checkBoxDrink3 = (CheckBox) findViewById(R.id.checkBoxDrink3);
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink3)).getCount() == Constants.MAX_DRINKS_SUPPLIES) {
            checkBoxDrink3.setChecked(false);
            checkBoxDrink3.setEnabled(false);
        }
        // CheckBoxes for stuff buying
        // Washing Agent
        CheckBox checkBoxWashingAgent = (CheckBox) findViewById(R.id.checkBoxWashingAgent);
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityClean1)).getCount() == Constants.MAX_STUFF_SUPPLIES) {
            checkBoxWashingAgent.setChecked(false);
            checkBoxWashingAgent.setEnabled(false);
        }
        // Dishsoap
        CheckBox checkBoxDishsoap = (CheckBox) findViewById(R.id.checkBoxDishsoap);
        if (Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityClean2)).getCount() == Constants.MAX_STUFF_SUPPLIES) {
            checkBoxDishsoap.setChecked(false);
            checkBoxDishsoap.setEnabled(false);
        }
    }

    /* ---------------------------------------------------------------------------
      disableCheckboxesAfterBuyButtonPressed() method will disable all checkboxes.
      It is used to prevent the user from manipulating his selection after buy process is started.
      ----------------------------------------------------------------------------*/
    public void disableCheckboxesAfterBuyButtonPressed() {
        CheckBox checkBoxMeal1 = (CheckBox) findViewById(R.id.checkBoxMeal1);
        CheckBox checkBoxMeal2 = (CheckBox) findViewById(R.id.checkBoxMeal2);
        CheckBox checkBoxMeal3 = (CheckBox) findViewById(R.id.checkBoxMeal3);
        CheckBox checkBoxMeal4 = (CheckBox) findViewById(R.id.checkBoxMeal4);
        CheckBox checkBoxMeal5 = (CheckBox) findViewById(R.id.checkBoxMeal5);
        CheckBox checkBoxMeal6 = (CheckBox) findViewById(R.id.checkBoxMeal6);
        CheckBox checkBoxMeal7 = (CheckBox) findViewById(R.id.checkBoxMeal7);
        CheckBox checkBoxMeal8 = (CheckBox) findViewById(R.id.checkBoxMeal8);
        CheckBox checkBoxMeal9 = (CheckBox) findViewById(R.id.checkBoxMeal9);
        CheckBox checkBoxDrink1 = (CheckBox) findViewById(R.id.checkBoxDrink1);
        CheckBox checkBoxDrink2 = (CheckBox) findViewById(R.id.checkBoxDrink2);
        CheckBox checkBoxDrink3 = (CheckBox) findViewById(R.id.checkBoxDrink3);
        CheckBox checkBoxWashingAgent = (CheckBox) findViewById(R.id.checkBoxWashingAgent);
        CheckBox checkBoxDishSoap = (CheckBox) findViewById(R.id.checkBoxDishsoap);

        //disable every checkbox
        checkBoxMeal1.setEnabled(false);
        checkBoxMeal2.setEnabled(false);
        checkBoxMeal3.setEnabled(false);
        checkBoxMeal4.setEnabled(false);
        checkBoxMeal5.setEnabled(false);
        checkBoxMeal6.setEnabled(false);
        checkBoxMeal7.setEnabled(false);
        checkBoxMeal8.setEnabled(false);
        checkBoxMeal9.setEnabled(false);
        checkBoxDrink1.setEnabled(false);
        checkBoxDrink2.setEnabled(false);
        checkBoxDrink3.setEnabled(false);
        checkBoxWashingAgent.setEnabled(false);
        checkBoxDishSoap.setEnabled(false);

    }

    /* ---------------------------------------------------------------------------
    removeSelectionAfterBuyProcessFinished() unchecks all checkboxes.
    It is used to uncheck all checkboxes after buy is finished because
    the checkboxes will remain checked while buy process is running
    to display the items which will be bought.
    ----------------------------------------------------------------------------*/
    public void removeSelectionAfterBuyProcessFinished() {
        // all checkboxes in the store
        CheckBox checkBoxMeal1 = (CheckBox) findViewById(R.id.checkBoxMeal1);
        CheckBox checkBoxMeal2 = (CheckBox) findViewById(R.id.checkBoxMeal2);
        CheckBox checkBoxMeal3 = (CheckBox) findViewById(R.id.checkBoxMeal3);
        CheckBox checkBoxMeal4 = (CheckBox) findViewById(R.id.checkBoxMeal4);
        CheckBox checkBoxMeal5 = (CheckBox) findViewById(R.id.checkBoxMeal5);
        CheckBox checkBoxMeal6 = (CheckBox) findViewById(R.id.checkBoxMeal6);
        CheckBox checkBoxMeal7 = (CheckBox) findViewById(R.id.checkBoxMeal7);
        CheckBox checkBoxMeal8 = (CheckBox) findViewById(R.id.checkBoxMeal8);
        CheckBox checkBoxMeal9 = (CheckBox) findViewById(R.id.checkBoxMeal9);
        CheckBox checkBoxDrink1 = (CheckBox) findViewById(R.id.checkBoxDrink1);
        CheckBox checkBoxDrink2 = (CheckBox) findViewById(R.id.checkBoxDrink2);
        CheckBox checkBoxDrink3 = (CheckBox) findViewById(R.id.checkBoxDrink3);
        CheckBox checkBoxWashingAgent = (CheckBox) findViewById(R.id.checkBoxWashingAgent);
        CheckBox checkBoxDishSoap = (CheckBox) findViewById(R.id.checkBoxDishsoap);

        // unchecks all checkboxes
        checkBoxMeal1.setChecked(false);
        checkBoxMeal2.setChecked(false);
        checkBoxMeal3.setChecked(false);
        checkBoxMeal4.setChecked(false);
        checkBoxMeal5.setChecked(false);
        checkBoxMeal6.setChecked(false);
        checkBoxMeal7.setChecked(false);
        checkBoxMeal8.setChecked(false);
        checkBoxMeal9.setChecked(false);
        checkBoxDrink1.setChecked(false);
        checkBoxDrink2.setChecked(false);
        checkBoxDrink3.setChecked(false);
        checkBoxWashingAgent.setChecked(false);
        checkBoxDishSoap.setChecked(false);
    }

    /* ---------------------------------------------------------------------------
    checkDoubleEntriesInSelection() removes duplicates in the ArrayList selections.
    ----------------------------------------------------------------------------*/
    public void checkDoubleEntriesInSelection(ArrayList<Item> selections) {
        HashSet<Item> hashSet = new HashSet<>(selections);
        selections.clear();
        selections.addAll(hashSet);
    }
}

