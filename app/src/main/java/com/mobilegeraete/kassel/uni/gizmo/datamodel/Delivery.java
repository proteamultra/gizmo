/*
   Copyright (c) 2017 alehi
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.mobilegeraete.kassel.uni.gizmo.datamodel;

import java.beans.PropertyChangeSupport;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;

import com.google.gson.annotations.Expose;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Dlw;

public  class Delivery{

   
   //==========================================================================
   
   protected PropertyChangeSupport listeners = null;

   
   public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue)
   {
      if (listeners != null) {
   		listeners.firePropertyChange(propertyName, oldValue, newValue);
   		return true;
   	}
   	return false;
   }
   
   public boolean addPropertyChangeListener(PropertyChangeListener listener) 
   {
   	if (listeners == null) {
   		listeners = new PropertyChangeSupport(this);
   	}
   	listeners.addPropertyChangeListener(listener);
   	return true;
   }
   
   public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
   	if (listeners == null) {
   		listeners = new PropertyChangeSupport(this);
   	}
   	listeners.addPropertyChangeListener(propertyName, listener);
   	return true;
   }
   
   public boolean removePropertyChangeListener(PropertyChangeListener listener) {
   	if (listeners == null) {
   		listeners.removePropertyChangeListener(listener);
   	}
   	listeners.removePropertyChangeListener(listener);
   	return true;
   }

   public boolean removePropertyChangeListener(String propertyName,PropertyChangeListener listener) {
   	if (listeners != null) {
   		listeners.removePropertyChangeListener(propertyName, listener);
   	}
   	return true;
   }

   
   //==========================================================================



   public void removeYou()
   {
      setDlw(null);
      firePropertyChange("REMOVE_YOU", this, null);
   }

   
   /********************************************************************
    * <pre>
    *              one                       one
    * Delivery ----------------------------------- Dlw
    *              delivery                   dlw
    * </pre>
    */
   
   public static final String PROPERTY_DLW = "dlw";

   private Dlw dlw = null;

   public Dlw getDlw()
   {
      return this.dlw;
   }

   public boolean setDlw(Dlw value)
   {
      boolean changed = false;
      
      if (this.dlw != value)
      {
         Dlw oldValue = this.dlw;
         
         if (this.dlw != null)
         {
            this.dlw = null;
            oldValue.setDelivery(null);
         }
         
         this.dlw = value;
         
         if (value != null)
         {
            value.withDelivery(this);
         }
         
         firePropertyChange(PROPERTY_DLW, oldValue, value);
         changed = true;
      }
      
      return changed;
   }

   public Delivery withDlw(Dlw value)
   {
      setDlw(value);
      return this;
   }

    //------------------------------------------------------

    public static final String PROPERTY_TIME = "time";

    @Expose
    private long time;

    public long getTime() {
        return this.time;
    }

    public void setTime(long value) {

        if (this.time != value) {

            long oldValue = this.time;
            this.time = value;
            this.firePropertyChange(PROPERTY_TIME, oldValue, value);
        }
    }

    public Delivery withTime(long value) {
        setTime(value);
        return this;
    }

    //----------------------------------------------------------

    @Expose
    private ArrayList<String> items = new ArrayList<String>();

    public ArrayList<String> getItems() {
        return items;
    }

    public void setItems(ArrayList<String> items) {
        this.items = items;
    }
}
