package com.mobilegeraete.kassel.uni.gizmo.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.view.View;

import com.mobilegeraete.kassel.uni.gizmo.Constants;
import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.Variables;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;

public class TestingItems extends AppCompatActivity {
    NotificationCompat.Builder notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing_goods);

        //Set up actionBar for TestingItems activity
        //Retrieve a reference to this activity's ActionBar.
        ActionBar actionBar = getSupportActionBar();
        //Set up the apps logo in the actionbar.
        actionBar.setLogo(R.mipmap.dummy);
        //Set up the activities title in the action bar.
        actionBar.setTitle(getString(R.string.testingItemsTitle));
        //Set whether to display the activity logo rather than the activity icon.
        actionBar.setDisplayUseLogoEnabled(true);
        //Set whether to include the application home affordance in the action bar.
        actionBar.setDisplayShowHomeEnabled(true);

        notification = new NotificationCompat.Builder(this);
        notification.setAutoCancel(true);

    }

    public void fillGoods(View v) {
        switch (v.getId()) {
            //Meals
            case R.id.testGoodsFillMeal1:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal1)).setCount(Constants.MAX_MEALS_SUPPLIES);
                break;
            case R.id.testGoodsFillMeal2:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal2)).setCount(Constants.MAX_MEALS_SUPPLIES);
                break;
            case R.id.testGoodsFillMeal3:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal3)).setCount(Constants.MAX_MEALS_SUPPLIES);
                break;
            case R.id.testGoodsFillMeal4:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal4)).setCount(Constants.MAX_MEALS_SUPPLIES);
                break;
            case R.id.testGoodsFillMeal5:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal5)).setCount(Constants.MAX_MEALS_SUPPLIES);
                break;
            case R.id.testGoodsFillMeal6:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal6)).setCount(Constants.MAX_MEALS_SUPPLIES);
                break;
            case R.id.testGoodsFillMeal7:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal7)).setCount(Constants.MAX_MEALS_SUPPLIES);
                break;
            case R.id.testGoodsFillMeal8:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal8)).setCount(Constants.MAX_MEALS_SUPPLIES);
                break;
            case R.id.testGoodsFillMeal9:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal9)).setCount(Constants.MAX_MEALS_SUPPLIES);
                break;

            // Drinks
            case R.id.testGoodsFillDrink1:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink1)).setCount(Constants.MAX_DRINKS_SUPPLIES);
                break;
            case R.id.testGoodsFillDrink2:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink2)).setCount(Constants.MAX_DRINKS_SUPPLIES);
                break;
            case R.id.testGoodsFillDrink3:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink3)).setCount(Constants.MAX_DRINKS_SUPPLIES);
                break;
            // Stuff
            case R.id.testGoodsFillWashingAgent:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityClean1)).setCount(Constants.MAX_STUFF_SUPPLIES);
                break;
            case R.id.testGoodsFillDishSoap:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityClean2)).setCount(Constants.MAX_STUFF_SUPPLIES);
                break;
            case R.id.testGoodsCleanDaySuit:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityCloth1)).setCount(Constants.MAX_STUFF_SUPPLIES);
                break;
            case R.id.testGoodsCleanNightSuit:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityCloth2)).setCount(Constants.MAX_STUFF_SUPPLIES);
                break;
            case R.id.testGoodsCleanCup:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityCup)).setCount(Constants.MAX_STUFF_SUPPLIES);
                break;
            case R.id.testGoodsCleanPlate:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityPlate)).setCount(Constants.MAX_STUFF_SUPPLIES);
                break;
        }
    }

    public void emptyStorage(View v) {
        switch (v.getId()) {
            //Meals
            case R.id.testGoodsEmptyMeal1:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal1)).setCount(0);
                break;
            case R.id.testGoodsEmptyMeal2:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal2)).setCount(0);
                break;
            case R.id.testGoodsEmptyMeal3:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal3)).setCount(0);
                break;
            case R.id.testGoodsEmptyMeal4:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal4)).setCount(0);
                break;
            case R.id.testGoodsEmptyMeal5:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal5)).setCount(0);
                break;
            case R.id.testGoodsEmptyMeal6:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal6)).setCount(0);
                break;
            case R.id.testGoodsEmptyMeal7:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal7)).setCount(0);
                break;
            case R.id.testGoodsEmptyMeal8:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal8)).setCount(0);
                break;
            case R.id.testGoodsEmptyMeal9:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal9)).setCount(0);
                break;
            // Drinks
            case R.id.testGoodsEmptyDrink1:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink1)).setCount(0);
                break;
            case R.id.testGoodsEmptyDrink2:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink2)).setCount(0);
                break;
            case R.id.testGoodsEmptyDrink3:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink3)).setCount(0);
                break;
            // Stuff
            case R.id.testGoodsEmptyWashingAgent:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityClean1)).setCount(0);
                break;
            case R.id.testGoodsEmptyDishSoap:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityClean2)).setCount(0);
                break;
            case R.id.testGoodsDirtyDaySuit:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityCloth1)).setCount(0);
                break;
            case R.id.testGoodsDirtyNightSuit:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityCloth2)).setCount(0);
                break;
            case R.id.testGoodsDirtyCup:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityCup)).setCount(0);
                break;
            case R.id.testGoodsDirtyPlate:
                Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityPlate)).setCount(0);
                break;
        }

    }

    public void goToMainMenu(View v) {
        Intent intent = new Intent(this, MainMenu.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        // To prevent an additional LOOP thread the switch back to mainMenu has to be saved
        Variables.backToMainMenu = true;
    }
}
