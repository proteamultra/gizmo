package com.mobilegeraete.kassel.uni.gizmo.activities;

import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobilegeraete.kassel.uni.gizmo.BackgroundService;
import com.mobilegeraete.kassel.uni.gizmo.Constants;
import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Cleaning;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Clothing;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Cup;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Dlw;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Drinks;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Food;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Item;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Plate;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Storage;
import com.mobilegeraete.kassel.uni.gizmo.util.LifecycleHandler;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;

public class StartActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        //kill service
        stopService(new Intent(this, BackgroundService.class));

        //register Lifecycle Callback
        this.getApplication().registerActivityLifecycleCallbacks(LifecycleHandler.getInstance());

        //try to load dataset
        if (Persistence.loadDlw(this)) {
            Intent intent = new Intent(this, MainMenu.class);

            //check forwarding extra from Notifications
            String to = getIntent().getStringExtra("forwardTo");
            if (to != null) {

                //cancel all active Notifications
                NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                nm.cancelAll();

                Log.d("forwarding", "startActivity");
                intent.putExtra("forwardTo", to);
            }

            startActivity(intent);

        }
    }

    //The Methods reads the text, the user put into the EditText widget. Creates the dlw,
    //stores the StartTime, calls the initItems method and saves the DLW for the first time.
    public void startGame(View v) {

        //Get text from editText widget
        EditText txtname = (EditText) findViewById(R.id.startActivityNameET);
        //Display a toast incase the user put in an invalid username.
        if (txtname.getText().toString().isEmpty()) {

            Toast t = Toast.makeText(getApplicationContext(),
                    R.string.startActivityInvalidUsernameT,
                    Toast.LENGTH_LONG);
            t.show();

        } else {
            //If the username is valid, create a new dlw object with the previous entered username
            Dlw dlw = new Dlw().withName(txtname.getText().toString());
            //Set the currentTime as the startTime of the game
            dlw.setCreated(System.currentTimeMillis());
            //Create game objects
            dlw.withStorage(new Storage());
            //Create all items
            iniItems(dlw);

            dlw.initNeeds(this);
            //Stores the dlw object for the first time.
            Persistence.setDlw(this, dlw);

            //Creates an intent and switches to the mainMeanu activity
            Intent intent = new Intent(this, MainMenu.class);
            startActivity(intent);
        }
    }

    public void iniItems(Dlw dlw) {
        //---------------------- Creating food-objects --------------------
        //--------- create food objects ---------
        Item beans = new Food().withName(this.getString(R.string.startActivityMeal1)).withType(this.getString(R.string.startActivityBreakfastFoodType)).withCount(1);
        dlw.getStorage().withItems(beans);
        Item crispies = new Food().withName(this.getString(R.string.startActivityMeal2)).withType(this.getString(R.string.startActivityBreakfastFoodType)).withCount(1);
        dlw.getStorage().withItems(crispies);
        Item wurzelwurst = new Food().withName(this.getString(R.string.startActivityMeal3)).withType(this.getString(R.string.startActivityBreakfastFoodType)).withCount(1);
        dlw.getStorage().withItems(wurzelwurst);
        Item eisbein = new Food().withName(this.getString(R.string.startActivityMeal4)).withType(this.getString(R.string.startActivityLunchFoodType)).withCount(1);
        dlw.getStorage().withItems(eisbein);
        Item sulperknochen = new Food().withName(this.getString(R.string.startActivityMeal5)).withType(this.getString(R.string.startActivityLunchFoodType)).withCount(1);
        dlw.getStorage().withItems(sulperknochen);
        Item greensauce = new Food().withName(this.getString(R.string.startActivityMeal6)).withType(this.getString(R.string.startActivityLunchFoodType)).withCount(1);
        dlw.getStorage().withItems(greensauce);
        Item beaconcake = new Food().withName(this.getString(R.string.startActivityMeal7)).withType(this.getString(R.string.startActivityDinnerFoodType)).withCount(1);
        dlw.getStorage().withItems(beaconcake);
        Item ahlewurscht = new Food().withName(this.getString(R.string.startActivityMeal8)).withType(this.getString(R.string.startActivityDinnerFoodType)).withCount(1);
        dlw.getStorage().withItems(ahlewurscht);
        Item hackbroetchen = new Food().withName(this.getString(R.string.startActivityMeal9)).withType(this.getString(R.string.startActivityDinnerFoodType)).withCount(1);
        dlw.getStorage().withItems(hackbroetchen);

        //---------------------- Creating drink-objects --------------------
        //--------- create drink objects ---------
        Item cacao = new Drinks().withName(this.getString(R.string.startActivityDrink1)).withType(this.getString(R.string.startActivityFoodTypeDrink)).withCount(1);
        dlw.getStorage().withItems(cacao);
        Item wheatbeer = new Drinks().withName(this.getString(R.string.startActivityDrink2)).withType(this.getString(R.string.startActivityFoodTypeDrink)).withCount(1);
        dlw.getStorage().withItems(wheatbeer);
        Item cornschnapps = new Drinks().withName(this.getString(R.string.startActivityDrink3)).withType(this.getString(R.string.startActivityFoodTypeDrink)).withCount(1);
        dlw.getStorage().withItems(cornschnapps);

        //---------------------- Creating stuff-objects --------------------
        //--------- create stuff-objects ---------
        Item washingAgent = new Cleaning().withName(this.getString(R.string.startActivityClean1)).withType(this.getString(R.string.startActivityFoodTypeWashingAgent)).withCount(Constants.MAX_STUFF_SUPPLIES);
        dlw.getStorage().withItems(washingAgent);
        Item dishSoap = new Cleaning().withName(this.getString(R.string.startActivityClean2)).withType(this.getString(R.string.startActivityTypeDishSoap)).withCount(Constants.MAX_STUFF_SUPPLIES);
        dlw.getStorage().withItems(dishSoap);
        Item daySuit = new Clothing().withName(this.getString(R.string.startActivityCloth1)).withType(this.getString(R.string.startActivityTypeSuit)).withCount(Constants.MAX_STUFF_SUPPLIES);
        dlw.getStorage().withItems(daySuit);
        Item nightSuit = new Clothing().withName(this.getString(R.string.startActivityCloth2)).withType(this.getString(R.string.startActivityTypeSuit)).withCount(Constants.MAX_STUFF_SUPPLIES);
        dlw.getStorage().withItems(nightSuit);
        Item cup = new Cup().withName(this.getString(R.string.startActivityCup)).withType(this.getString(R.string.startActivityTypeDishes)).withCount(Constants.MAX_STUFF_SUPPLIES);
        dlw.getStorage().withItems(cup);
        Item plate = new Plate().withName(this.getString(R.string.startActivityPlate)).withType(this.getString(R.string.startActivityTypeDishes)).withCount(Constants.MAX_STUFF_SUPPLIES);
        dlw.getStorage().withItems(plate);

        //TMP DISH NO DRINK!
        //muss ersetzt werden, brauchen aber nicht cup/plates sondern allg. dishes reicht
        //Item dishes = new Drinks().withName(this.getString(R.string.startActivityDishes)).withType(this.getString(R.string.startActivityTypeDishes)).withCount(Constants.MAX_STUFF_SUPPLIES);
        //dlw.getStorage().withItems(dishes);
    }
}
