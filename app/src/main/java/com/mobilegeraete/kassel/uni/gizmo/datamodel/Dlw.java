/*
   Copyright (c) 2016 alehi
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.mobilegeraete.kassel.uni.gizmo.datamodel;

import android.content.Context;

import com.google.gson.annotations.Expose;
import com.mobilegeraete.kassel.uni.gizmo.Constants;
import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Dlw {


    //==========================================================================

    public static final String PROPERTY_NAME = "name";
    public static final String PROPERTY_CREATED = "created";
    public static final String PROPERTY_DEATH_TIME = "deathTime";
    public static final String PROPERTY_NEXTNEEDS = "nextNeeds";
    public static final String PROPERTY_NEEDS = "needs";
    public static final String PROPERTY_STORAGE = "storage";
    public static final String PROPERTY_DELIVERY = "delivery";
    public static final String PROPERTY_BUYPROCESSSTARTED = "buyProcessStarted";

    protected PropertyChangeSupport listeners = null;
    @Expose
    private String name;
    @Expose
    private long created;
    @Expose
    private long deathTime;
    @Expose
    private boolean buyProcessStarted = false;


    private List<Needs> needs = Collections.synchronizedList(new ArrayList<Needs>());

    private List<Needs> nextNeeds = Collections.synchronizedList(new ArrayList<Needs>());

    private Storage storage = null;

    private Delivery delivery = null;



    public void initNeeds(Context context){
        createNextDressingNeed(context);
        createNextPlayNeed(context);
        createNextThirst(context);
        createNextHunger(context);
        createNextWalkNeed();
    }


    public void createNextPlayNeed(Context context){
        Random r = new Random(System.currentTimeMillis());
        int random= r.nextInt();
        if(random<0) random*=-1;

        int secondesTillNextPlay = random % (Constants.MAX_SECONDES_TILL_NEXT_PLAY
                -Constants.MIN_SECONDES_TILL_NEXT_PLAY+1);
        secondesTillNextPlay = secondesTillNextPlay + Constants.MIN_SECONDES_TILL_NEXT_PLAY;

        //get timestamp for Play event start
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.SECOND, secondesTillNextPlay);

        Play play = new Play();
        play.withStartTime(calendar.getTimeInMillis());
        play.setRequiredItem("" + random % Constants.NO_OF_SAYINGS);

        this.withNextNeeds(play);

        System.out.println("NextPlay erstellt: " + play);



    }

    public void createNextWalkNeed(){
        Random r = new Random(System.currentTimeMillis());
        int random= r.nextInt();
        if(random<0) random*=-1;

        int minutesTillNextWalk = random%(Constants.MAX_MINUTES_TILL_NEXT_WALK
                -Constants.MIN_MINUTES_TILL_NEXT_WALK +1);
        minutesTillNextWalk = minutesTillNextWalk + Constants.MIN_MINUTES_TILL_NEXT_WALK;

        //get timestamp for Walk-event start
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.add(Calendar.MINUTE, minutesTillNextWalk);

        Walk walk = new Walk();
        walk.withStartTime(calendar.getTimeInMillis());

        this.withNextNeeds(walk);

        System.out.println("NextWalk erstellt: " + walk);



    }

    public void createNextDressingNeed(Context context){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        Dress dress = new Dress();

        //next is for the evening
        if (hour < Constants.DRESS_HOUR_2
                && hour >= Constants.DRESS_HOUR_1){
                calendar.set(Calendar.HOUR_OF_DAY, Constants.DRESS_HOUR_2);
                dress.setRequiredItem(context.getString(R.string.startActivityCloth2));

        }
        //next is at the morning
        else{

            //today morning
            if(hour<Constants.DRESS_HOUR_1){
                calendar.set(Calendar.HOUR_OF_DAY, Constants.DRESS_HOUR_1);
            }
            //tomorrow morning
            else{
                calendar.set(Calendar.HOUR_OF_DAY, 24+Constants.DRESS_HOUR_1);
            }

            dress.setRequiredItem(context.getString(R.string.startActivityCloth1));
        }

        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        dress.setStartTime(calendar.getTimeInMillis());

        withNextNeeds(dress);

        System.out.println("NextDress erstellt: "+ dress);
    }

    public void createNextHunger(Context context){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        ArrayList<String> meals = new ArrayList<>();

        Random r = new Random(System.currentTimeMillis());
        int random= r.nextInt();
        if(random<0) random*=-1;
        random=random%3;

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        Hunger hunger = new Hunger();

        //breakfast
        if (hour < Constants.HUNGER_HOUR_1
                || hour >= Constants.HUNGER_HOUR_3){

            //today
            if(hour < Constants.HUNGER_HOUR_1){
                calendar.set(Calendar.HOUR_OF_DAY, Constants.HUNGER_HOUR_1);
            }
            //tomorrow
            else if( hour >= Constants.HUNGER_HOUR_3){
                calendar.set(Calendar.HOUR_OF_DAY, 24+Constants.HUNGER_HOUR_1);
            }
            meals.add(context.getString(R.string.startActivityMeal1));
            meals.add(context.getString(R.string.startActivityMeal2));
            meals.add(context.getString(R.string.startActivityMeal3));

        }
        //lunch
        else if(hour<Constants.HUNGER_HOUR_2 && hour>= Constants.HUNGER_HOUR_1){
            calendar.set(Calendar.HOUR_OF_DAY, Constants.HUNGER_HOUR_2);

            meals.add(context.getString(R.string.startActivityMeal4));
            meals.add(context.getString(R.string.startActivityMeal5));
            meals.add(context.getString(R.string.startActivityMeal6));



        }
        //dinner
        else{
            calendar.set(Calendar.HOUR_OF_DAY, Constants.HUNGER_HOUR_3);

            meals.add(context.getString(R.string.startActivityMeal7));
            meals.add(context.getString(R.string.startActivityMeal8));
            meals.add(context.getString(R.string.startActivityMeal9));

        }

        hunger.setRequiredItem(meals.get(random));
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        hunger.setStartTime(calendar.getTimeInMillis());

        withNextNeeds(hunger);

        System.out.println("NextHunger erstellt: "+ hunger);
    }

    public void createNextThirst(Context context){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());

        Random r = new Random(System.currentTimeMillis());
        int random= r.nextInt();
        if(random<0) random*=-1;
        random=random%3;


        //choose a random drink
        ArrayList<String> drinks = new ArrayList<>();
        drinks.add(context.getString(R.string.startActivityDrink1));
        drinks.add(context.getString(R.string.startActivityDrink2));
        drinks.add(context.getString(R.string.startActivityDrink3));

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        Thirst thirst = new Thirst();

        //THIRSTHOUR_1
        if (hour < Constants.THIRST_HOUR_1
                || hour >= Constants.THIRST_HOUR_5){

            //today
            if(hour < Constants.THIRST_HOUR_1){
                calendar.set(Calendar.HOUR_OF_DAY, Constants.THIRST_HOUR_1);
            }
            //tomorrow
            else if( hour >= Constants.THIRST_HOUR_5){
                calendar.set(Calendar.HOUR_OF_DAY, 24+Constants.THIRST_HOUR_1);
            }
        }
        //THIRSTHOUR_2
        else if(hour<Constants.THIRST_HOUR_2 && hour>= Constants.THIRST_HOUR_1){
            calendar.set(Calendar.HOUR_OF_DAY, Constants.THIRST_HOUR_2);
        }
        //THIRSTHOUR_3
        else if(hour<Constants.THIRST_HOUR_3 && hour>= Constants.THIRST_HOUR_2){
            calendar.set(Calendar.HOUR_OF_DAY, Constants.THIRST_HOUR_3);
        }
        //THIRSTHOUR_4
        else if(hour<Constants.THIRST_HOUR_4 && hour>= Constants.THIRST_HOUR_3){
            calendar.set(Calendar.HOUR_OF_DAY, Constants.THIRST_HOUR_4);
        }
        //THIRSTHOUR_5
        else{
            calendar.set(Calendar.HOUR_OF_DAY, Constants.THIRST_HOUR_5);
        }

        thirst.setRequiredItem(drinks.get(random));
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        thirst.setStartTime(calendar.getTimeInMillis());

        withNextNeeds(thirst);

        System.out.println("NextThirst erstellt: "+ thirst);
    }

    //==========================================================================

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (listeners != null) {
            listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if (listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(listener);
        return true;
    }

    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if (listeners == null) {
            listeners.removePropertyChangeListener(listener);
        }
        listeners.removePropertyChangeListener(listener);
        return true;
    }

    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listeners != null) {
            listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    public void removeYou() {
        withoutNeeds(this.getNeeds().toArray(new Needs[this.getNeeds().size()]));
        withoutNextNeeds(this.getNextNeeds().toArray(new Needs[this.getNextNeeds().size()]));
        setStorage(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }

    public String getName() {
        return this.name;
    }

    public void setName(String value) {

        if (value == null || !value.equals(this.name)) {

            String oldValue = this.name;
            this.name = value;
            this.firePropertyChange(PROPERTY_NAME, oldValue, value);
        }
    }

    public Dlw withName(String value) {
        setName(value);
        return this;
    }

    public long getCreated() {
        return this.created;
    }

    public void setCreated(long value) {

        if (this.created != value) {

            long oldValue = this.created;
            this.created = value;
            this.firePropertyChange(PROPERTY_CREATED, oldValue, value);
        }
    }

    public Dlw withCreated(long value) {
        setCreated(value);
        return this;
    }

    public long getDeathTime() {
        return this.deathTime;
    }

    public void setDeathTime(long value) {

        if (this.deathTime != value) {

            long oldValue = this.deathTime;
            this.deathTime = value;
            this.firePropertyChange(PROPERTY_DEATH_TIME, oldValue, value);
        }
    }

    public Dlw withDeathTime(long value) {
        setDeathTime(value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append(" ").append(this.getName());
        result.append(" ").append(this.getCreated());
        result.append(" ").append(this.isBuyProcessStarted());
        return result.substring(1);
    }

    public List<Needs> getNeeds() {
        if (this.needs == null) {
            return null;
        }

        return this.needs;
    }

    public Dlw withNeeds(Needs... value) {
        if (value == null) {
            return this;
        }
        for (Needs item : value) {
            if (item != null) {
                if (this.needs == null) {
                    this.needs = new ArrayList<Needs>();
                }

                boolean changed = false;
                if(!this.needs.contains(item)){
                   changed = this.needs.add(item);
                }


                if (changed) {
                    item.withDlw(this);
                    firePropertyChange(PROPERTY_NEEDS, null, item);
                }
            }
        }
        return this;
    }

    public Dlw withoutNeeds(Needs... value) {
        for (Needs item : value) {
            if ((this.needs != null) && (item != null)) {
                if (this.needs.remove(item)) {
                    item.setDlw(null);
                    firePropertyChange(PROPERTY_NEEDS, item, null);
                }
            }
        }
        return this;
    }

    public List<Needs> getNextNeeds() {
        if (this.nextNeeds == null) {
            return null;
        }

        return this.nextNeeds;
    }

    public Dlw withNextNeeds(Needs... value) {
        if (value == null) {
            return this;
        }
        for (Needs item : value) {
            if (item != null) {
                if (this.nextNeeds == null) {
                    this.nextNeeds = new ArrayList<Needs>();
                }

                boolean changed = false;
                if(!this.nextNeeds.contains(item)){
                    changed = this.nextNeeds.add(item);
                }


                if (changed) {
                    item.withDlw2(this);
                    firePropertyChange(PROPERTY_NEXTNEEDS, null, item);
                }
            }
        }
        return this;
    }

    public Dlw withoutNextNeeds(Needs... value) {
        for (Needs item : value) {
            if ((this.nextNeeds != null) && (item != null)) {
                if (this.nextNeeds.remove(item)) {
                    item.setDlw2(null);
                    firePropertyChange(PROPERTY_NEXTNEEDS, item, null);
                }
            }
        }
        return this;
    }

    public Storage getStorage() {
        return this.storage;
    }

    public boolean setStorage(Storage value) {
        boolean changed = false;

        if (this.storage != value) {
            Storage oldValue = this.storage;

            if (this.storage != null) {
                this.storage = null;
                oldValue.setDlw(null);
            }

            this.storage = value;

            if (value != null) {
                value.withDlw(this);
            }

            firePropertyChange(PROPERTY_STORAGE, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Dlw withStorage(Storage value) {
        setStorage(value);
        return this;
    }


    public Delivery getDelivery() {
        return this.delivery;
    }

    public boolean setDelivery(Delivery value) {
        boolean changed = false;

        if (this.delivery != value) {
            Delivery oldValue = this.delivery;

            if (this.delivery != null) {
                this.delivery = null;
                oldValue.setDlw(null);
            }

            this.delivery = value;

            if (value != null) {
                value.withDlw(this);
            }

            firePropertyChange(PROPERTY_DELIVERY, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Dlw withDelivery(Delivery value) {
        setDelivery(value);
        return this;
    }

    public boolean isNotDead() {

        for (Needs need : Persistence.getDlw().getNextNeeds()) {

                //if deadline is exceeded..
                if(need.getDeadline() <= System.currentTimeMillis()){
                        return false;
                }

        }


        for (Needs need : Persistence.getDlw().getNeeds()) {

            //if deadline is exceeded..
            if(need.getDeadline() <= System.currentTimeMillis()){
                return false;
            }
        }
        return true;
    }


    public boolean isBuyProcessStarted() {
        return buyProcessStarted;
    }

    public void setBuyProcessStarted(boolean value) {

        if (this.buyProcessStarted != value) {

            boolean oldValue = this.buyProcessStarted;
            this.buyProcessStarted = value;
            this.firePropertyChange(PROPERTY_BUYPROCESSSTARTED, oldValue, value);
        }

    }
}
