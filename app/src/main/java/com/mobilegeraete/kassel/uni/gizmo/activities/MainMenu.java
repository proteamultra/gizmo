package com.mobilegeraete.kassel.uni.gizmo.activities;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.mobilegeraete.kassel.uni.gizmo.Constants;
import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.Variables;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Dlw;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Dress;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Hunger;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Item;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Needs;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Play;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Thirst;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Walk;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import pl.droidsonroids.gif.GifImageView;

public class MainMenu extends AppCompatActivity {

    private static final long NEEDS_LOOP_RHYTHM = 1000;
    private static final long BUY_LOOP_RHYTHM = 1000;


    private CountDownTimer countDownTimer;
    private CountDownTimer countDownTimerBuyProcess;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        //make sure dlw is loaded
        Persistence.loadDlw(this);
        //clear all existing and pending Notifications
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.cancelAll();

        final Activity currentActivity = this;

        //Set up actionBar for mainMenu activity
        //Retrieve a reference to this activity's ActionBar.
        ActionBar actionBar = getSupportActionBar();
        //Set up the apps logo in the actionbar.
        actionBar.setLogo(R.mipmap.happy);
        //Set up the activities title in the action bar.
        actionBar.setTitle(getString(R.string.mainMenu));
        //Set whether to display the activity logo rather than the activity icon.
        actionBar.setDisplayUseLogoEnabled(true);
        //Set whether to include the application home affordance in the action bar.
        actionBar.setDisplayShowHomeEnabled(true);
        //actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#9bb7e8")));


        Dlw dlw = Persistence.getDlw();

        if (dlw != null) {
            final GifImageView dlwGif = (GifImageView) findViewById(R.id.dlwImageView);
            //statusText.setText(dlw.getName() + " ist happy!");
            changeStatus(dlwGif);

            List needs = dlw.getNeeds();

            ListAdapter adapter = new MainMenuAdapter(this, needs);
            //ListView nLV = (ListView)findViewById(R.id.needsListView);
            ListView nLV = (ListView) findViewById(R.id.mainMenuNeedsLV);
            nLV.setAdapter(adapter);

            //The user can click on an item in the list view. The listener fires, when the user clicks on an item,
            //creates an intent and switches to a new activity. It depends an the clicked item, which activity is started.
            nLV.setOnItemClickListener(
                    new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                            Needs tmpNeed = (Needs) adapterView.getItemAtPosition(position);

                            if (tmpNeed instanceof Play) {
                                Intent intent = new Intent(currentActivity, LetsPlay.class);
                                startActivity(intent);
                            } else if (tmpNeed instanceof Thirst) {
                                Intent intent = new Intent(currentActivity, Actions.class);
                                startActivity(intent);
                            } else if (tmpNeed instanceof Hunger) {
                                Intent intent = new Intent(currentActivity, Actions.class);
                                startActivity(intent);
                            } else if (tmpNeed instanceof Dress) {
                                Intent intent = new Intent(currentActivity, Actions.class);
                                startActivity(intent);
                            } else if (tmpNeed instanceof Walk) {
                                Intent intent = new Intent(currentActivity, TakingAWalk.class);
                                startActivity(intent);
                            }
                        }
                    }
            );

            //PropertyChangeListener to update the listview when new needs are added or when needs are deleted.
            dlw.addPropertyChangeListener(dlw.PROPERTY_NEEDS, new PropertyChangeListener() {
                @Override
                public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                    //Toast.makeText(MainMenu.this, "Neues Bedürfnis", Toast.LENGTH_SHORT).show();

                    Dlw dlw = Persistence.getDlw();
                    if (dlw != null) {
                        List needs = dlw.getNeeds();

                        ListAdapter adapter = new MainMenuAdapter(getApplicationContext(), needs);
                        ListView nLV = (ListView) findViewById(R.id.mainMenuNeedsLV);
                        nLV.setAdapter(adapter);

                        //Change status
                        changeStatus(dlwGif);

                    }
                }
            });

        /* ---------------------------------------------------------------------------
        When the user left the shop via the backToMainMenuButton, or other methods
        which interact with the MainMenu.class the startBuyLoop will not be called again to
        prevent more than one loop running at a time.
        The first if will always be entered on the first start of the app.

        When there is a delivery object the remainingTime will be set
        and the buyLoop will continue with the buy process.

        When there is no delivery object the buyLoop will start and "wait" for a delivery object.
        ----------------------------------------------------------------------------*/

            if (!Variables.backToMainMenu) {
                if (Persistence.getDlw().getDelivery() != null && Persistence.getDlw().isBuyProcessStarted()) {
                    Log.d("LOOP", "HABEN DELIVERY dlw variable true");
                    long remainingDeliveryTime = Persistence.getDlw().getDelivery().getTime();
                    Variables.remainingTime = remainingDeliveryTime - System.currentTimeMillis();
                    startBuyLoop();
                    //Only to make sure, this case do no harm. No additional loop will start.
                } else if (Persistence.getDlw().getDelivery() != null && !Persistence.getDlw().isBuyProcessStarted()) {

                    //Only to make sure, this case do no harm. No additional loop will start.
                } else if (Persistence.getDlw().getDelivery() == null && Persistence.getDlw().isBuyProcessStarted()) {

                } else {
                    Log.d("LOOP", "KEIN DELIVERY OBJECT");
                    startBuyLoop();
                }
            }


            //checks forwarding extra, added by the BackgroundService into the Notification intent
            //forwards to another activity
            String to = (String) getIntent().getStringExtra("forwardTo");
            if (to != null) {
                if (!to.equals("mainMenu")) {
                    Log.d("forwarding", "mainActivity");
                    Intent intent = new Intent(this, Actions.class);
                    intent.putExtra("forwardTo", to);
                    startActivity(intent);
                }
            }
        }
    }


    public void onPause() {
        super.onPause();
    }

    //If the needs list is empty the dlws status is "happy" and the happy gif is being loaded from the resources.
    // The status changes to unhappy when there are needs added to the list. The angry gif is being loaded from the resources.
    private void changeStatus(GifImageView gifImage) {
        //Load the current dlw
        Dlw dlw = Persistence.getDlw();
        //Check if dlw is null to prevent NullPointerException.
        if (dlw != null) {
            //Get the List where all the needs are stored.
            List needs = dlw.getNeeds();
            //Check is the needs list is empty or not.
            if (needs.isEmpty()) {
                //Load happy gif
                gifImage.setImageResource(R.mipmap.happy);

            } else {
                //Load unhappy gif
                gifImage.setImageResource(R.mipmap.angry);
            }
        }
    }

    public void goToSuppliesActivity(View v) {
        //   Log.i(TAG, "fire");
        Intent intent = new Intent(this, Supplies.class);
        startActivity(intent);
    }

    public void goToTestingActivity(View v) {
        Intent intent = new Intent(this, Testing.class);
        startActivity(intent);
    }

    public void goToActions(View v) {
        Intent intent = new Intent(this, Actions.class);
        startActivity(intent);
    }

    public void goToStoreActivity(View v) {
        Intent intent = new Intent(this, Store.class);
        startActivity(intent);
    }

    /**
     * Tracks the start-time and deadline of all current and future needs.
     */
    public void startNeedsLoop() {

        final Activity mainMenuActivity = this;

        countDownTimer = new CountDownTimer(Long.MAX_VALUE, NEEDS_LOOP_RHYTHM) {
            @Override
            public void onTick(long l) {

                if (Persistence.getDlw() == null) {
                    countDownTimer.cancel();
                    return;
                }

                //temporary stores needs, to remove them after iteration
                ArrayList<Needs> needsToRemove = new ArrayList<Needs>();


                //check future NextNeeds
                if (Persistence.getDlw().getNextNeeds() != null) {

                    for (Needs need : Persistence.getDlw().getNextNeeds()) {

                        //if currentTime reached startTime of NextNeed...
                        if (need.getStartTime() <= System.currentTimeMillis()) {

                            //.. move it to NeedsList..
                            Persistence.getDlw().withNeeds(need);
                            //.. and remove it after iteration
                            needsToRemove.add(need);


                        }
                    }

                    //remove NextNeeds
                    for (Needs need : needsToRemove) {
                        Persistence.getDlw().withoutNextNeeds(need);

                        //creates the nextNeed only if the starting need wasn't generated by tests
                        if (!need.getGeneratedByTest()) {
                            if (need instanceof Dress) {
                                Persistence.getDlw().createNextDressingNeed(mainMenuActivity);
                            } else if (need instanceof Play) {
                                Persistence.getDlw().createNextPlayNeed(mainMenuActivity);
                            } else if (need instanceof Hunger) {
                                Persistence.getDlw().createNextHunger(mainMenuActivity);
                            } else if (need instanceof Thirst) {
                                Persistence.getDlw().createNextThirst(mainMenuActivity);
                            } else if (need instanceof Walk) {
                                Persistence.getDlw().createNextWalkNeed();
                            }
                        }
                    }

                    needsToRemove.clear();

                }

                //check current Needs
                if (Persistence.getDlw().getNeeds() != null) {

                    for (Needs need : Persistence.getDlw().getNeeds()) {

                        //if currentTime reached deadline of Need...
                        if (need.getDeadline() <= System.currentTimeMillis()) {
                            //remove it after iteration

                            //ACTIVATE FOR NORMAL DEATH
                            if (countDownTimerBuyProcess != null) {
                                countDownTimerBuyProcess.cancel();
                            }

                            Persistence.getDlw().setDeathTime(need.getDeadline());

                            Intent intent = new Intent(mainMenuActivity, DeathActivity.class);
                            startActivity(intent);

                            return;

                            //ACTIVATE FOR IMMORTAL MODE
                            //needsToRemove.add(need);
                        }
                    }

                    //remove NextNeeds
                    for (Needs need : needsToRemove) {
                        Persistence.getDlw().withoutNeeds(need);
                    }

                    needsToRemove.clear();

                }
            }

            @Override
            public void onFinish() {

            }
        }.start();
    }


    /* ---------------------------------------------------------------------------
    The startBuyLoop() method is handling the buy process of items.
    The loop is running all the time when there is no delivery it is not doing anything.
    The delivery object is created as soon as the user presses the buy button in the store.
    If the user exited the app after starting a buy process and
    starts the app again the loop will be called in the onCreate.
    It will restart the buy process with the remaining time.
    ----------------------------------------------------------------------------*/
    public void startBuyLoop() {

        if (Persistence.getDlw() == null) {
            countDownTimerBuyProcess.cancel();
            return;
        }

        countDownTimerBuyProcess = new CountDownTimer(Long.MAX_VALUE, BUY_LOOP_RHYTHM) {
            public void onTick(long millisUntilFinished) {

                if (Persistence.getDlw() == null) {
                    countDownTimerBuyProcess.cancel();
                    return;
                }

                //check if a delivery is existing and a buy process currently running
                if (Persistence.getDlw().getDelivery() != null && Persistence.getDlw().isBuyProcessStarted()) {
                    Log.d("LOOP", "es gibt eine Delivery");
                    Log.d("LOOP", "deliveryTime" + Variables.remainingTime);
                    // if the remaining time is above one second it will be reduced by a second
                    // if it is already below one second the buy process will be finished
                    // and the item counter will be incremented
                    if (Variables.remainingTime > 1000) {
                        Log.d("LOOP", "remainingTime über null");
                        Variables.remainingTime = Variables.remainingTime - 1000;
                        Variables.buyProcessRunning = true;
                        Persistence.getDlw().getDelivery().withTime(Variables.remainingTime);
                    } else {
                        Log.d("LOOP", "Delivery abgeschlossen");
                        for (Item item : Persistence.getDlw().getStorage().getItems()) {
                            for (String name : Persistence.getDlw().getDelivery().getItems()) {
                                if (item.getName().equals(name)) {
                                    Log.d("LOOP", "Counter vor dem hochsetzen" + item.getCount());
                                    item.setCount(item.getCount() + 1);
                                    Log.d("LOOP", "Counter nach dem hochsetzen" + item.getCount());
                                }
                            }
                        }
                        // set variables to start values and remove finished delivery
                        Variables.remainingTime = Constants.SHOPPING_DURATION;
                        Variables.selections.clear();
                        Persistence.getDlw().setBuyProcessStarted(false);
                        Variables.buyProcessRunning = false;
                        Persistence.getDlw().setDelivery(null);
                    }
                }
            }

            // on finish only if counter terminates while shop activity is open.
            public void onFinish() {
            }
        }.start();
    }

    @Override
    protected void onStop() {
        countDownTimer.cancel();
        super.onStop();

    }

    @Override
    protected void onStart() {
        Log.d("LOOP", "ONSTART");
        //Start internal Timer Loop
        startNeedsLoop();
        super.onStart();
    }

    //Implement the actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Add menu.xml into the actionbar
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //This method is called whenever an item (settings) in your options menu is selected.
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Create an intent to open the Settings activity
        Intent intent = new Intent(this, Settings.class);
        startActivity(intent);
        return super.onOptionsItemSelected(item);
    }
}
