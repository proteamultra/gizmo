package com.mobilegeraete.kassel.uni.gizmo.datamodel;

import android.content.Context;

import com.mobilegeraete.kassel.uni.gizmo.Constants;
import com.mobilegeraete.kassel.uni.gizmo.R;

/**
 * Created by alehi on 17.01.2017.
 */

public class Walk extends Needs{


    //==========================================================================

    @Override
    public void removeYou() {
        setDlw(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }

    @Override
    public String getNeedInfo(Context context) {
        //String walk = "Mir ist langweilig. Lass uns spazieren gehen!";
        String walk = context.getString(R.string.walkInfo);
        return walk;
    }


    @Override
    public long getDeadline() {
        return getStartTime() + Constants.SATISFY_WALK;
    }
}
