package com.mobilegeraete.kassel.uni.gizmo;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import com.mobilegeraete.kassel.uni.gizmo.activities.MainMenu;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Dress;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Hunger;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Needs;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Play;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Thirst;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Walk;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


/**
 * Created by alehi on 08.01.2017.
 */

public class BackgroundService extends Service {

    private CountDownTimer cdt;
    private int uniqueID = 1337;
    private final int LOOP_RHYTHM = 10000;

    //stores Needs, where the notification has already been pushed. Prevents duplicate Notifications
    private ArrayList<Needs> ignoreList = new ArrayList<Needs>();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("BackgroundService ", "BackgroundService called");
        if (!Persistence.loadDlw(this)) {
            onDestroy();
        } else {
            startBackgroundLoop();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("BackgroundService", "BackgroundService -- onStartCommand()");

        // We want this service to continue running until it is explicitly
        // stopped, so we return sticky.
        return START_STICKY;
    }

    @Override
    public void onDestroy() {

        if (cdt != null) {
            cdt.cancel();
        }
        Log.d("BackgroundService ", "BackgroundService destroyed");
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    /**
     * Starts a Loop to check the deadline and startTimes of the Dlw's Need Objects.
     * Whenever a Need starts, or exceeds the deadline, a notification is generated and pushed.
     * The Loop stops when the Service is destroyed.
     */
    private void startBackgroundLoop() {

        final Service thisService = this;

        cdt = new CountDownTimer(Long.MAX_VALUE, LOOP_RHYTHM) {

            //indicates the death of the Dlw, the Loop will no longer check the Needs if set to true
            //this prevents duplicate 'dlwDied' notifications
            private boolean isDead = false;

            @Override
            public void onTick(long l) {

                if (isDead) return;

                if (Persistence.getDlw().getNextNeeds() != null) {

                    Log.d("BackgroundService ", "BackgroundService checkt NextNeeds..");
                    for (Needs need : Persistence.getDlw().getNextNeeds()) {

                        if (ignoreList.contains(need)) continue;

                        Log.d("BackgroundService ", need + "   :: Systemtime: " + System.currentTimeMillis());
                        //if currentTime reached startTime of NextNeed...
                        if (need.getStartTime() <= System.currentTimeMillis()) {

                            //.. and if deadline is exceeded..
                            if (need.getDeadline() <= System.currentTimeMillis()) {

                                //..push "dlw died" notification and set 'isDead'
                                pushDlwDiedNotification();
                                isDead = true;
                                return;
                            }
                            //.. else push a "new Need" notification
                            else {

                                pushNewNeedNotification(need);
                                ignoreList.add(need);
                            }
                        }
                    }
                }


                if (Persistence.getDlw().getNeeds() != null) {
                    Log.d("BackgroundService ", "BackgroundService checkt Needs..");
                    for (Needs need : Persistence.getDlw().getNeeds()) {
                        if (ignoreList.contains(need)) continue;

                        //if currentTime reached deadline of Need...
                        if (need.getDeadline() <= System.currentTimeMillis()) {
                            //..push "dlw died" notification and set 'isDead'
                            pushDlwDiedNotification();
                            isDead = true;
                            return;
                        }
                    }
                }

            }

            @Override
            public void onFinish() {

            }
        }.start();
    }

    /**
     * Generates and pushes a 'dlw Died' Notification
     */
    private void pushDlwDiedNotification() {
        Intent intent = new Intent(this, MainMenu.class);
        // To prevent an additional LOOP thread the switch back to mainMenu has to be saved
        Variables.backToMainMenu = true;
        intent.putExtra("forwardTo", "mainMenu");
        String text = Persistence.getDlw().getName() + " ist tot!";
        pushNotification(text, intent);
        onDestroy();
    }

    /**
     * Generates and pushes a customized 'new Need' Notification for the @param need
     *
     * @param need The need
     */
    private void pushNewNeedNotification(Needs need) {

        Log.d("BackgroundService", "pushing notification");
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date resultStartTime = new Date(need.getStartTime());
        Date resultDeadline = new Date(need.getDeadline());
        String text = "" + sdf.format(resultStartTime) + " " + "" + need.getNeedInfo(getApplicationContext()) + " " + "" + sdf.format(resultDeadline);

        Intent intent = new Intent(this, MainMenu.class);
        // To prevent an additional LOOP thread the switch back to mainMenu has to be saved
        Variables.backToMainMenu = true;

        //set forwarding extra
        if (need instanceof Dress) {
            intent.putExtra("forwardTo", "actions");
        } else if (need instanceof Play) {
            intent.putExtra("forwardTo", "play");
        } else if (need instanceof Hunger) {
            intent.putExtra("forwardTo", "actions");
        } else if (need instanceof Thirst) {
            intent.putExtra("forwardTo", "actions");
        } else if (need instanceof Walk) {
            intent.putExtra("forwardTo", "takingAWalk");
        }

        pushNotification(text, intent);
    }

    /**
     * Pushes a Notification via the NotificationBuilder
     *
     * @param text   The notification content text
     * @param intent The intent inside of the Notification
     */
    private void pushNotification(String text, Intent intent) {

        //brings any currently running instance of that task (our Application) to the foreground, and then clears it to its root state
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);

        NotificationCompat.Builder notification = new NotificationCompat.Builder(this);

        //If setAutoCancel is true the notification will be canceled after opening the app
        notification.setAutoCancel(true);
        notification.setSmallIcon(R.mipmap.happy);
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle(text);
        notification.setDefaults(Notification.DEFAULT_VIBRATE);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, uniqueID, intent, 0);
        notification.setContentIntent(pendingIntent);

        //Builds notification and issues it
        NotificationManager nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        nm.notify(uniqueID, notification.build());
        uniqueID++;
    }

}
