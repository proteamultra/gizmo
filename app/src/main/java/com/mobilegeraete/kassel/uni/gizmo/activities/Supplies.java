package com.mobilegeraete.kassel.uni.gizmo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import com.mobilegeraete.kassel.uni.gizmo.Constants;
import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Item;
import com.mobilegeraete.kassel.uni.gizmo.util.Persistence;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class Supplies extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_supplies);

        //Set up actionBar for Supplies activity
        //Retrieve a reference to this activity's ActionBar.
        ActionBar actionBar = getSupportActionBar();
        //Set up the apps logo in the actionbar.
        actionBar.setLogo(R.mipmap.stock);
        //Set up the activities title in the action bar.
        actionBar.setTitle(getString(R.string.suppliesTitle));
        //Set whether to display the activity logo rather than the activity icon.
        actionBar.setDisplayUseLogoEnabled(true);
        //Set whether to include the application home affordance in the action bar.
        actionBar.setDisplayShowHomeEnabled(true);

       /* ---------------------------------------------------------------------------
        Tabs in the supply activity containing three tabs for Food, Drink and Household.
        ----------------------------------------------------------------------------*/
        TabHost host = (TabHost) findViewById(R.id.tabHost);
        host.setup();

        //Tab 1 Food
        TabHost.TabSpec spec = host.newTabSpec("tab1");
        spec.setContent(R.id.suppliesTab1);
        spec.setIndicator("");
        host.addTab(spec);
        host.getTabWidget().getChildAt(0).setBackgroundResource(R.mipmap.pizza);

        //Tab 2 Drink
        spec = host.newTabSpec("tab2");
        spec.setContent(R.id.suppliesTab2);
        spec.setIndicator("");
        host.addTab(spec);
        host.getTabWidget().getChildAt(1).setBackgroundResource(R.mipmap.cocktail);

        //Tab 3 Household
        spec = host.newTabSpec("tab3");
        spec.setContent(R.id.suppliesTab3);
        spec.setIndicator("");
        host.addTab(spec);
        host.getTabWidget().getChildAt(2).setBackgroundResource(R.mipmap.wash);

        // fillTabs() call to fill tabs with content
        fillTabs();

        // for each loop traversing the items of the dlw adding a listener to each item
        for (Item item : Persistence.getDlw().getStorage().getItems()) {
            Log.d("Listener", "bin ich drin");
            if (item != null) {
                item.addPropertyChangeListener(new PropertyChangeListener() {
                    @Override
                    public void propertyChange(PropertyChangeEvent propertyChangeEvent) {
                        fillTabs();
                    }
                });
            }
        }

    }


    protected void onResume() {
        super.onResume();
        fillTabs();
    }

    // onClick method to switch to store activity
    public void goToStore(View v) {
        Intent intentToGoToTheStore = new Intent(this, Store.class);
        startActivity(intentToGoToTheStore);
    }


    /* ---------------------------------------------------------------------------
    Method used to fill tabs with content
    ----------------------------------------------------------------------------*/
    public void fillTabs() {
        // TextViews to display the current amount in stock
        // TextViews for Food
        TextView countMeal1 = (TextView) findViewById(R.id.numberMeal1);
        TextView countMeal2 = (TextView) findViewById(R.id.numberMeal2);
        TextView countMeal3 = (TextView) findViewById(R.id.numberMeal3);
        TextView countMeal4 = (TextView) findViewById(R.id.numberMeal4);
        TextView countMeal5 = (TextView) findViewById(R.id.numberMeal5);
        TextView countMeal6 = (TextView) findViewById(R.id.numberMeal6);
        TextView countMeal7 = (TextView) findViewById(R.id.numberMeal7);
        TextView countMeal8 = (TextView) findViewById(R.id.numberMeal8);
        TextView countMeal9 = (TextView) findViewById(R.id.numberMeal9);

        // TextViews for Drink
        TextView countDrink1 = (TextView) findViewById(R.id.countDrink1);
        TextView countDrink2 = (TextView) findViewById(R.id.countDrink2);
        TextView countDrink3 = (TextView) findViewById(R.id.countDrink3);

        // TextViews for Household
        TextView countWashingAgent = (TextView) findViewById(R.id.countWashingAgent);
        TextView countDishSoap = (TextView) findViewById(R.id.countDishSoap);
        TextView countDaySuit = (TextView) findViewById(R.id.countDaySuit);
        TextView countNightSuit = (TextView) findViewById(R.id.countNightSuit);
        TextView countCup = (TextView) findViewById(R.id.countCup);
        TextView countPlate = (TextView) findViewById(R.id.countPlate);

        // Add corresponding count in stock and capacity
        // FOOD
        // Meal 1
        Item meal1 = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal1));
        int countMeal1Int = meal1.getCount();
        String cm1s = String.valueOf(countMeal1Int);
        countMeal1.setText(cm1s + "/" + Constants.MAX_MEALS_SUPPLIES);
        // Meal 2
        Item meal2 = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal2));
        int countMeal2Int = meal2.getCount();
        String cm2s = String.valueOf(countMeal2Int);
        countMeal2.setText(cm2s + "/" + Constants.MAX_MEALS_SUPPLIES);
        // Meal 3
        Item meal3 = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal3));
        int countMeal3Int = meal3.getCount();
        String cm3s = String.valueOf(countMeal3Int);
        countMeal3.setText(cm3s + "/" + Constants.MAX_MEALS_SUPPLIES);
        // Meal 4
        Item meal4 = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal4));
        int countMeal4Int = meal4.getCount();
        String cm4s = String.valueOf(countMeal4Int);
        countMeal4.setText(cm4s + "/" + Constants.MAX_MEALS_SUPPLIES);
        // Meal 5
        Item meal5 = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal5));
        int countMeal5Int = meal5.getCount();
        String cm5s = String.valueOf(countMeal5Int);
        countMeal5.setText(cm5s + "/" + Constants.MAX_MEALS_SUPPLIES);
        // Meal 6
        Item meal6 = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal6));
        int countMeal6Int = meal6.getCount();
        String cm6s = String.valueOf(countMeal6Int);
        countMeal6.setText(cm6s + "/" + Constants.MAX_MEALS_SUPPLIES);
        // Meal 7
        Item meal7 = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal7));
        int countMeal7Int = meal7.getCount();
        String cm7s = String.valueOf(countMeal7Int);
        countMeal7.setText(cm7s + "/" + Constants.MAX_MEALS_SUPPLIES);
        // Meal 8
        Item meal8 = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal8));
        int countMeal8Int = meal8.getCount();
        String cm8s = String.valueOf(countMeal8Int);
        countMeal8.setText(cm8s + "/" + Constants.MAX_MEALS_SUPPLIES);
        // Meal 9
        Item meal9 = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityMeal9));
        int countMeal9Int = meal9.getCount();
        String cm9s = String.valueOf(countMeal9Int);
        countMeal9.setText(cm9s + "/" + Constants.MAX_MEALS_SUPPLIES);

        // DRINK
        // Drink 1
        Item drink1 = Persistence.getDlw().getStorage().findItem(getString(R.string.startActivityDrink1));
        int countDrink1Int = drink1.getCount();
        String cd1s = String.valueOf(countDrink1Int);
        countDrink1.setText(cd1s + "/" + Constants.MAX_DRINKS_SUPPLIES);
        // Drink 2
        Item drink2 = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink2));
        int countDrink2Int = drink2.getCount();
        String cd2s = String.valueOf(countDrink2Int);
        countDrink2.setText(cd2s + "/" + Constants.MAX_DRINKS_SUPPLIES);
        // Drink 3
        Item drink3 = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityDrink3));
        int countDrink3Int = drink3.getCount();
        String cd3s = String.valueOf(countDrink3Int);
        countDrink3.setText(cd3s + "/" + Constants.MAX_DRINKS_SUPPLIES);

        // Household
        //Household washingAgent
        Item washingAgent = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityClean1));
        int countStuff1Int = washingAgent.getCount();
        String cs1s = String.valueOf(countStuff1Int);
        countWashingAgent.setText(cs1s + "/" + Constants.MAX_STUFF_SUPPLIES);
        //Household dishSoap
        Item dishSoap = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityClean2));
        int countStuff2Int = dishSoap.getCount();
        String cs2s = String.valueOf(countStuff2Int);
        countDishSoap.setText(cs2s + "/" + Constants.MAX_STUFF_SUPPLIES);
        //Household daySuit
        Item daySuit = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityCloth1));
        int countStuff3Int = daySuit.getCount();
        String cs3s = String.valueOf(countStuff3Int);
        countDaySuit.setText(cs3s + "/" + Constants.MAX_STUFF_SUPPLIES);
        //Household nightSuit
        Item nightSuit = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityCloth2));
        int countStuff4Int = nightSuit.getCount();
        String cs4s = String.valueOf(countStuff4Int);
        countNightSuit.setText(cs4s + "/" + Constants.MAX_STUFF_SUPPLIES);
        //Household cup
        Item cup = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityCup));
        int countStuff5Int = cup.getCount();
        String cs5s = String.valueOf(countStuff5Int);
        countCup.setText(cs5s + "/" + Constants.MAX_STUFF_SUPPLIES);
        //Household plate
        Item plate = Persistence.getDlw().getStorage().findItem(this.getString(R.string.startActivityPlate));
        int countStuff6Int = plate.getCount();
        String cs6s = String.valueOf(countStuff6Int);
        countPlate.setText(cs6s + "/" + Constants.MAX_STUFF_SUPPLIES);
    }
}
