package com.mobilegeraete.kassel.uni.gizmo.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mobilegeraete.kassel.uni.gizmo.R;
import com.mobilegeraete.kassel.uni.gizmo.datamodel.Needs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


/**
 * Created by Thorsten on 09.01.2017.
 */

class MainMenuAdapter extends ArrayAdapter<Needs> {
    public MainMenuAdapter(Context context, List<Needs> needs) {
        super(context, R.layout.main_menu_list_view_custom_row, needs);
    }

    //@NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(getContext());
        View customView = inflater.inflate(R.layout.main_menu_list_view_custom_row, parent, false);
        TextView timeTextView = (TextView) customView.findViewById(R.id.timeTextView);
        TextView needTextView = (TextView) customView.findViewById(R.id.needTextView);
        TextView deadlineTextView = (TextView) customView.findViewById(R.id.deadlineTextView);

        Needs singleNeed = getItem(position);

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Date resultDate = new Date(singleNeed.getStartTime());

        timeTextView.setText("" + sdf.format(resultDate));

        //needTextView.setText(singleNeed.getNeedInfo() + " Ich will " + singleNeed.getRequiredItem() + "!");
        needTextView.setText(singleNeed.getNeedInfo(getContext()));

        resultDate = new Date(singleNeed.getDeadline());
        deadlineTextView.setText("" + sdf.format(resultDate));

        return customView;
    }
}
