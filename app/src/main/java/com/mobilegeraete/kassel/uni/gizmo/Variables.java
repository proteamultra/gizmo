package com.mobilegeraete.kassel.uni.gizmo;

import com.mobilegeraete.kassel.uni.gizmo.datamodel.Item;

import java.util.ArrayList;

/**
 * Created by Ash on 10.01.2017.
 */

public class Variables {
    public static long remainingTime = Constants.SHOPPING_DURATION;
    public static boolean buyProcessRunning = false;
    public static boolean countDownTimerActive = false;
    public static boolean backToMainMenu = false;
    public static boolean buyButtonTimerActive = false;
    // need to start at counter = 1 , because onClickMethod checked counter internally
    public static int countSelectedItems = 1;
    public static ArrayList<Item> selections = new ArrayList<>();

    //Counter to estimate when the laundry is done
    public static int shakeCount = 0;
    public static int wipeCount = 0;


    //public static int stepCount = 0;

    public static int startStepCountValue = 0;
    public static int diffStepCountValue = 0;
}
