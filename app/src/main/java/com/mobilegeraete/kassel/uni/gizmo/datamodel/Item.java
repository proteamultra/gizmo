/*
   Copyright (c) 2016 alehi
   
   Permission is hereby granted, free of charge, to any person obtaining a copy of this software 
   and associated documentation files (the "Software"), to deal in the Software without restriction, 
   including without limitation the rights to use, copy, modify, merge, publish, distribute, 
   sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is 
   furnished to do so, subject to the following conditions: 
   
   The above copyright notice and this permission notice shall be included in all copies or 
   substantial portions of the Software. 
   
   The Software shall be used for Good, not Evil. 
   
   THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING 
   BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND 
   NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, 
   DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
   OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. 
 */

package com.mobilegeraete.kassel.uni.gizmo.datamodel;

import com.google.gson.annotations.Expose;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;

public class Item {


    //==========================================================================

    public static final String PROPERTY_NAME = "name";
    public static final String PROPERTY_TYPE = "type";
    public static final String PROPERTY_COUNT = "count";
    /********************************************************************
     * <pre>
     *              many                       one
     * Item ----------------------------------- Storage
     *              items                   storage
     * </pre>
     */

    public static final String PROPERTY_STORAGE = "storage";
    protected PropertyChangeSupport listeners = null;
    @Expose
    private String name;
    @Expose
    private String type;
    @Expose
    private int count;


    //==========================================================================

    private Storage storage = null;


    //==========================================================================

    public boolean firePropertyChange(String propertyName, Object oldValue, Object newValue) {
        if (listeners != null) {
            listeners.firePropertyChange(propertyName, oldValue, newValue);
            return true;
        }
        return false;
    }

    public boolean addPropertyChangeListener(PropertyChangeListener listener) {
        if (listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(listener);
        return true;
    }

    public boolean addPropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listeners == null) {
            listeners = new PropertyChangeSupport(this);
        }
        listeners.addPropertyChangeListener(propertyName, listener);
        return true;
    }

    public boolean removePropertyChangeListener(PropertyChangeListener listener) {
        if (listeners == null) {
            listeners.removePropertyChangeListener(listener);
        }
        listeners.removePropertyChangeListener(listener);
        return true;
    }

    public boolean removePropertyChangeListener(String propertyName, PropertyChangeListener listener) {
        if (listeners != null) {
            listeners.removePropertyChangeListener(propertyName, listener);
        }
        return true;
    }

    public void removeYou() {
        setStorage(null);
        firePropertyChange("REMOVE_YOU", this, null);
    }


    //==========================================================================

    public int getCount() {
        return this.count;
    }

    public void setCount(int value) {

        if (this.count != value) {

            int oldValue = this.count;
            this.count = value;
            this.firePropertyChange(PROPERTY_COUNT, oldValue, value);
        }
    }

    public Item withCount(int value) {
        setCount(value);
        return this;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String value) {
        if (value == null || !value.equals(this.name)) {

            String oldValue = this.name;
            this.name = value;
            this.firePropertyChange(PROPERTY_NAME, oldValue, value);
        }
    }

    public Item withName(String value) {
        setName(value);
        return this;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();

        result.append(" ").append(this.getName());
        result.append(" ").append(this.getType());
        result.append(" ").append(this.getCount());
        return result.substring(1);
    }

    public String getType() {
        return this.type;
    }

    public void setType(String value) {
        if (value == null || !value.equals(this.type)) {

            String oldValue = this.type;
            this.type = value;
            this.firePropertyChange(PROPERTY_TYPE, oldValue, value);
        }
    }

    public Item withType(String value) {
        setType(value);
        return this;
    }

    public Storage getStorage() {
        return this.storage;
    }

    public boolean setStorage(Storage value) {
        boolean changed = false;

        if (this.storage != value) {
            Storage oldValue = this.storage;

            if (this.storage != null) {
                this.storage = null;
                oldValue.withoutItems(this);
            }

            this.storage = value;

            if (value != null) {
                value.withItems(this);
            }

            firePropertyChange(PROPERTY_STORAGE, oldValue, value);
            changed = true;
        }

        return changed;
    }

    public Item withStorage(Storage value) {
        setStorage(value);
        return this;
    }

    public Storage createStorage() {
        Storage value = new Storage();
        withStorage(value);
        return value;
    }
}
