package com.mobilegeraete.kassel.uni.gizmo.util;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mobilegeraete.kassel.uni.gizmo.BackgroundService;

/**
 * Created by alehi on 11.01.2017.
 *
 * This callback tracks the visibility of our Gizmo application.
 * Our Background service will start and stop, dependent of the application visibility.
 */

public class LifecycleHandler implements Application.ActivityLifecycleCallbacks {
    private int resumed;
    private int started;

    private final String DebugName = "GizmoLifecycleHandler";

    private boolean isVisible = false;
    private boolean isInForeground = false;

    private static LifecycleHandler instance;

    //Singleton
    public static LifecycleHandler getInstance() {
        if (instance == null) {
            instance = new LifecycleHandler();
        }

        return instance;
    }

    private LifecycleHandler() {
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {
    }

    @Override
    public void onActivityDestroyed(Activity activity) {
    }

    @Override
    public void onActivityResumed(Activity activity) {
        ++resumed;
        android.util.Log.w(DebugName, "onActivityResumed -> application is in foreground: " + (resumed > 0) + " (" + activity.getClass() + ")");
        setForeground((resumed > 0));
    }

    @Override
    public void onActivityPaused(Activity activity) {
        --resumed;
        android.util.Log.w(DebugName, "onActivityPaused -> application is in foreground: " + (resumed > 0) + " (" + activity.getClass() + ")");
        setForeground((resumed > 0));
    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {
    }

    @Override
    public void onActivityStarted(Activity activity) {
        ++started;
        android.util.Log.w(DebugName, "onActivityStarted -> application is visible: " + (started > 0) + " (" + activity.getClass() + ")");
        setVisible((started > 0), activity);
    }

    @Override
    public void onActivityStopped(Activity activity) {
        --started;
        android.util.Log.w(DebugName, "onActivityStopped -> application is visible: " + (started > 0) + " (" + activity.getClass() + ")");
        setVisible((started > 0), activity);

    }

    private void setVisible(boolean visible, Activity activity) {
        if (isVisible == visible) {
            // no change
            return;
        }

        // visibility changed
        isVisible = visible;
        android.util.Log.w(DebugName, "App Visiblility Changed -> application is visible: " + isVisible);

        //stop service
        if(isVisible){
            activity.stopService(new Intent(activity, BackgroundService.class));
        }
        //start service
        else{
            Context context = activity.getApplicationContext();
            Intent serviceIntent = new Intent(context, BackgroundService.class);
            context.startService(serviceIntent);
        }
    }

    private void setForeground(boolean inForeground) {
        if (isInForeground == inForeground) {
            // no change
            return;
        }

        // in foreground changed
        isInForeground = inForeground;
        android.util.Log.w(DebugName, "App In Foreground Changed -> application is in foreground: " + isInForeground);


    }

    public static boolean isApplicationVisible() {
        return LifecycleHandler.getInstance().started > 0;
    }

    public static boolean isApplicationInForeground() {
        return LifecycleHandler.getInstance().resumed > 0;
    }
}